package com.mygdx.game;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.TextureLoader;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapLayers;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.objects.EllipseMapObject;
import com.badlogic.gdx.maps.objects.PolygonMapObject;
import com.badlogic.gdx.maps.objects.PolylineMapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileSet;
import com.badlogic.gdx.maps.tiled.TiledMapTileSets;
import com.badlogic.gdx.maps.tiled.objects.TiledMapTileMapObject;
import com.badlogic.gdx.maps.tiled.tiles.AnimatedTiledMapTile;
import com.badlogic.gdx.maps.tiled.tiles.StaticTiledMapTile;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Polyline;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.*;
import com.mygdx.game.entity.system.ai.pathfinding.PathfindingSystem;
import com.mygdx.game.entity.system.ai.pathfinding.TiledGraph;
import com.mygdx.game.factory.EntityFactory;
import com.mygdx.game.screen.ui.EntityHealthBar;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.StringTokenizer;
import java.util.zip.GZIPInputStream;
import java.util.zip.InflaterInputStream;

public class LevelLoader {

    private static final int FLAG_FLIP_HORIZONTALLY = 0x80000000;
    private static final int FLAG_FLIP_VERTICALLY = 0x40000000;
    private static final int FLAG_FLIP_DIAGONALLY = 0x20000000;
    private static final int MASK_CLEAR = 0xE0000000;

    private final FileHandle levelFile;
    private final PooledEngine engine;
    private final AssetManager assetManager;
    private final Stage stage;
    public boolean convertObjectToTileSpace = false;
    private XmlReader xml = new XmlReader();
    private XmlReader.Element root;
    private boolean flipY = true;
    private int mapTileWidth;
    private int mapTileHeight;
    private int mapWidthInPixels;
    private int mapHeightInPixels;

    private boolean[][] walkability;

    private TiledMapTileSets tilesets = new TiledMapTileSets();
    private MapLayers tileLayers = new MapLayers();

    public LevelLoader(FileHandle levelFile, AssetManager assetManager, PooledEngine engine,
                       Stage stage) {
        this.levelFile = levelFile;
        this.assetManager = assetManager;
        this.engine = engine;
        this.stage = stage;
    }

    private static int[] getTileIds(XmlReader.Element element, int width, int height) {
        XmlReader.Element data = element.getChildByName("data");
        String encoding = data.getAttribute("encoding", null);
        if (encoding == null) { // no 'encoding' attribute means that the encoding is XML
            throw new GdxRuntimeException("Unsupported encoding (XML) for TMX Layer Data");
        }
        int[] ids = new int[width * height];
        if (encoding.equals("csv")) {
            String[] array = data.getText().split(",");
            for (int i = 0; i < array.length; i++) {
                ids[i] = (int) Long.parseLong(array[i].trim());
            }
        } else {
            if (encoding.equals("base64")) {
                InputStream is = null;
                try {
                    String compression = data.getAttribute("compression", null);
                    byte[] bytes = Base64Coder.decode(data.getText());
                    if (compression == null) {
                        is = new ByteArrayInputStream(bytes);
                    } else if (compression.equals("gzip")) {
                        is =
                                new BufferedInputStream(
                                        new GZIPInputStream(new ByteArrayInputStream(bytes),
                                                bytes.length));
                    } else if (compression.equals("zlib")) {
                        is =
                                new BufferedInputStream(new InflaterInputStream(
                                        new ByteArrayInputStream(bytes)));
                    } else {
                        throw new GdxRuntimeException(
                                "Unrecognised compression (" + compression +
                                        ") for TMX Layer Data");
                    }

                    byte[] temp = new byte[4];
                    for (int y = 0; y < height; y++) {
                        for (int x = 0; x < width; x++) {
                            int read = is.read(temp);
                            while (read < temp.length) {
                                int curr = is.read(temp, read, temp.length - read);
                                if (curr == -1) {
                                    break;
                                }
                                read += curr;
                            }
                            if (read != temp.length) {
                                throw new GdxRuntimeException(
                                        "Error Reading TMX Layer Data: Premature end of tile data");
                            }
                            ids[y * width + x] =
                                    unsignedByteToInt(temp[0]) | unsignedByteToInt(temp[1]) << 8
                                            | unsignedByteToInt(temp[2]) << 16 |
                                            unsignedByteToInt(temp[3]) << 24;
                        }
                    }
                } catch (IOException e) {
                    throw new GdxRuntimeException(
                            "Error Reading TMX Layer Data - IOException: " + e.getMessage());
                } finally {
                    StreamUtils.closeQuietly(is);
                }
            } else {
                // any other value of 'encoding' is one we're not aware of, probably a feature of a future version of Tiled
                // or another editor
                throw new GdxRuntimeException(
                        "Unrecognised encoding (" + encoding + ") for TMX Layer Data");
            }
        }

        return ids;
    }

    private static int unsignedByteToInt(byte b) {
        return b & 0xFF;
    }

    private static FileHandle getRelativeFileHandle(FileHandle file, String path) {
        StringTokenizer tokenizer = new StringTokenizer(path, "\\/");
        FileHandle result = file.parent();
        while (tokenizer.hasMoreElements()) {
            String token = tokenizer.nextToken();
            if (token.equals("..")) {
                result = result.parent();
            } else {
                result = result.child(token);
            }
        }
        return result;
    }

    public void load() throws IOException {
        xml = new XmlReader();
        root = xml.parse(levelFile);

        int mapWidth = root.getIntAttribute("width", 0);
        int mapHeight = root.getIntAttribute("height", 0);
        int tileWidth = root.getIntAttribute("tilewidth", 0);
        int tileHeight = root.getIntAttribute("tileheight", 0);

        mapTileWidth = tileWidth;
        mapTileHeight = tileHeight;
        mapWidthInPixels = mapWidth * tileWidth;
        mapHeightInPixels = mapHeight * tileHeight;

        XmlReader.Element propertiesEl = root.getChildByName("properties");
        if (propertiesEl != null) {
            // Load map properties
            MapProperties properties = new MapProperties();
            loadProperties(properties, propertiesEl);

            // analyze map properties
        }
        Array<XmlReader.Element> tilesets = root.getChildrenByName("tileset");
        for (XmlReader.Element element : tilesets) {
            TiledMapTileSet tileset = loadTileSet(element, levelFile, assetManager);
            this.tilesets.addTileSet(tileset);
            root.removeChild(element);
        }

        for (int i = 0, j = root.getChildCount(); i < j; i++) {
            XmlReader.Element element = root.getChild(i);
            String name = element.getName();
            if (name.equals("layer")) {
                TiledMapTileLayer tileLayer = loadTileLayer(element);
                tileLayers.add(tileLayer);

                loadTiles(tileLayer);
            } else if (name.equals("objectgroup")) {
                MapLayer objectLayer = loadObjectGroup(element);
                String type = objectLayer.getProperties().get("Type", String.class);
                if ("Physics".equalsIgnoreCase(type)) {
                    loadPhysics(objectLayer);
                } else if ("GameObjects".equalsIgnoreCase(type)) {
                    loadGameObjects(objectLayer);
                }
            }
        }

        // Generate walkable grid
        this.walkability = new boolean[mapWidth][mapHeight];
        for (boolean[] row : walkability) {
            Arrays.fill(row, true);
        }

        for (int i = 0; i < tileLayers.getCount(); i++) {
            TiledMapTileLayer tileLayer = (TiledMapTileLayer) tileLayers.get(i);
            for (int y = 0; y < mapHeight; y++) {
                for (int x = 0; x < mapWidth; x++) {
                    TiledMapTile tile = tileLayer.getCell(x, y).getTile();

                    // Get current cell walkability
                    boolean currentValue = walkability[x][y];

                    // Get walkability of the cell on this layer
                    boolean tileWalkable =
                            tile.getProperties().get("walkable", Boolean.class);

                    // If the cell is already unwalkable, continue
                    // Else if it's walkable, set it to true
                    // Else set it to false
                    if (currentValue) {
                        this.walkability[x][y] = tileWalkable;
                    }
                }
            }
        }

        // Create physics
        for (int y = 0; y < mapHeight; y++) {
            for (int x = 0; x < mapWidth; x++) {
                if (!walkability[x][y])
                    EntityFactory.getInstance().createRectangleCollision(x, y, 1f, 1f);
            }
        }
    }

    private void loadTiles(TiledMapTileLayer tileLayer) {
        TiledGraph graph = new TiledGraph(mapTileWidth, mapTileHeight);

        final int width = tileLayer.getWidth();
        final int height = tileLayer.getHeight();
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                TiledMapTileLayer.Cell cell = tileLayer.getCell(x, y);
                if (cell == null) {
                    continue;
                }

                TiledMapTile tile = cell.getTile();
                EntityFactory.getInstance()
                        .createTile(tile.getTextureRegion(), x, y, 5);

                // TODO : batch non walkable tiles for physics

                boolean walkable = tile.getProperties().get("walkable", false, Boolean.class);
                graph.addNode(x, y, walkable);

                // Initialize the pathfinder
                engine.getSystem(PathfindingSystem.class).setGraph(graph);
            }
        }
    }

    private void loadPhysics(MapLayer objectLayer) {
        final Vector2 tmp = new Vector2();
        for (MapObject object : objectLayer.getObjects()) {
            if (object instanceof RectangleMapObject) {
                Rectangle rectangle = ((RectangleMapObject) object).getRectangle();
                rectangle.getCenter(tmp);
                EntityFactory.getInstance()
                        .createRectangleCollision(
                                tmp.x,
                                tmp.y,
                                (rectangle.getWidth() * 0.5f),
                                (rectangle.getHeight() * 0.5f)
                        );
            } else {
                throw new IllegalStateException(
                        "Object of type " + object.getClass().getSimpleName() +
                                " is not supported");
            }
        }
    }

    private void loadGameObjects(MapLayer objectLayer) {
        for (MapObject object : objectLayer.getObjects()) {
            MapProperties properties = object.getProperties();
            String type = properties.get("type", String.class);
            if (type == null) {
                throw new IllegalStateException("No type defined for object " + object.getName());
            }

            if (type.equalsIgnoreCase("Spawn")) {
                String target = properties.get("Target", String.class);
                float x = properties.get("x", Float.class);
                float y = properties.get("y", Float.class);
                if (target.equalsIgnoreCase("Player")) {
                    com.mygdx.game.factory.EntityFactory.getInstance().createPlayer(
                            x * Constants.World.PPM_FACTOR,
                            y * Constants.World.PPM_FACTOR
                    );
                } else if (target.equalsIgnoreCase("Enemy")) {
                    Entity enemy = com.mygdx.game.factory.EntityFactory.getInstance().createEnemy(
                            x * Constants.World.PPM_FACTOR,
                            y * Constants.World.PPM_FACTOR,
                            4f
                    );

                    EntityHealthBar enemyHealthBar = stage.getRoot().findActor("enemyHealthBar");
                    if (enemyHealthBar != null) {
                        enemyHealthBar.setTarget(enemy);
                    } else {
                        throw new RuntimeException("Unable to find actor enemyHealthBar");
                    }
                } else {
                    throw new IllegalStateException("Target " + target + " is unknown");
                }
            }
        }
    }

    private TiledMapTileSet loadTileSet(XmlReader.Element element, FileHandle tmxFile,
                                        AssetManager assetManager) {
        if (!element.getName().equals("tileset")) {
            throw new IllegalStateException("No tileset found");
        }

        String name = element.get("name", null);
        int firstgid = element.getIntAttribute("firstgid", 1);
        int tilewidth = element.getIntAttribute("tilewidth", 0);
        int tileheight = element.getIntAttribute("tileheight", 0);
        int spacing = element.getIntAttribute("spacing", 0);
        int margin = element.getIntAttribute("margin", 0);
        String source = element.getAttribute("source", null);

        int offsetX = 0;
        int offsetY = 0;

        String imageSource = "";
        int imageWidth = 0, imageHeight = 0;

        FileHandle image = null;
        if (source != null) {
            FileHandle tsx = getRelativeFileHandle(tmxFile, source);
            element = xml.parse(tsx);
            name = element.get("name", null);
            tilewidth = element.getIntAttribute("tilewidth", 0);
            tileheight = element.getIntAttribute("tileheight", 0);
            spacing = element.getIntAttribute("spacing", 0);
            margin = element.getIntAttribute("margin", 0);
            XmlReader.Element offset = element.getChildByName("tileoffset");
            if (offset != null) {
                offsetX = offset.getIntAttribute("x", 0);
                offsetY = offset.getIntAttribute("y", 0);
            }
            XmlReader.Element imageElement = element.getChildByName("image");
            if (imageElement != null) {
                imageSource = imageElement.getAttribute("source");
                imageWidth = imageElement.getIntAttribute("width", 0);
                imageHeight = imageElement.getIntAttribute("height", 0);
                image = getRelativeFileHandle(tsx, imageSource);
            }
        } else {
            XmlReader.Element offset = element.getChildByName("tileoffset");
            if (offset != null) {
                offsetX = offset.getIntAttribute("x", 0);
                offsetY = offset.getIntAttribute("y", 0);
            }
            XmlReader.Element imageElement = element.getChildByName("image");
            if (imageElement != null) {
                imageSource = imageElement.getAttribute("source");
                imageWidth = imageElement.getIntAttribute("width", 0);
                imageHeight = imageElement.getIntAttribute("height", 0);
                image = getRelativeFileHandle(tmxFile, imageSource);
            }
        }

        TiledMapTileSet tileset = new TiledMapTileSet();
        tileset.setName(name);
        tileset.getProperties().put("firstgid", firstgid);
        if (image != null) {
            TextureLoader.TextureParameter params = new TextureLoader.TextureParameter();
            params.genMipMaps = false;
            params.magFilter = Texture.TextureFilter.Nearest;
            params.minFilter = Texture.TextureFilter.Nearest;
            params.wrapU = Texture.TextureWrap.MirroredRepeat;
            params.wrapV = Texture.TextureWrap.MirroredRepeat;
            assetManager.load(image.path(), Texture.class, params);
            assetManager.finishLoading();
            Texture texture = assetManager.get(image.path(), Texture.class);

            int stopWidth = texture.getWidth() - tilewidth;
            int stopHeight = texture.getHeight() - tileheight;

            int id = firstgid;

            for (int y = margin; y <= stopHeight; y += tileheight + spacing) {
                for (int x = margin; x <= stopWidth; x += tilewidth + spacing) {
                    TextureRegion tileRegion =
                            new TextureRegion(texture, x, y, tilewidth, tileheight);
                    TiledMapTile tile = new StaticTiledMapTile(tileRegion);
                    tile.setId(id);
                    tile.setOffsetX(offsetX);
                    tile.setOffsetY(flipY ? -offsetY : offsetY);
                    tileset.putTile(id++, tile);
                }
            }
        }
        Array<XmlReader.Element> tileElements = element.getChildrenByName("tile");

        Array<AnimatedTiledMapTile> animatedTiles = new Array<AnimatedTiledMapTile>();

        for (XmlReader.Element tileElement : tileElements) {
            int localtid = tileElement.getIntAttribute("id", 0);
            TiledMapTile tile = tileset.getTile(firstgid + localtid);
            if (tile != null) {
                XmlReader.Element animationElement = tileElement.getChildByName("animation");
                if (animationElement != null) {

                    Array<StaticTiledMapTile> staticTiles = new Array<StaticTiledMapTile>();
                    IntArray intervals = new IntArray();
                    for (XmlReader.Element frameElement : animationElement
                            .getChildrenByName("frame")) {
                        staticTiles.add((StaticTiledMapTile) tileset
                                .getTile(firstgid + frameElement.getIntAttribute("tileid")));
                        intervals.add(frameElement.getIntAttribute("duration"));
                    }

                    AnimatedTiledMapTile animatedTile =
                            new AnimatedTiledMapTile(intervals, staticTiles);
                    animatedTile.setId(tile.getId());
                    animatedTiles.add(animatedTile);
                    tile = animatedTile;
                }

                String terrain = tileElement.getAttribute("terrain", null);
                if (terrain != null) {
                    tile.getProperties().put("terrain", terrain);
                }
                String probability = tileElement.getAttribute("probability", null);
                if (probability != null) {
                    tile.getProperties().put("probability", probability);
                }
                XmlReader.Element properties = tileElement.getChildByName("properties");
                if (properties != null) {
                    loadProperties(tile.getProperties(), properties);
                }
            }
        }

        for (AnimatedTiledMapTile tile : animatedTiles) {
            tileset.putTile(tile.getId(), tile);
        }

        XmlReader.Element properties = element.getChildByName("properties");
        if (properties != null) {
            loadProperties(tileset.getProperties(), properties);
        }

        return tileset;
    }

    private TiledMapTileLayer loadTileLayer(XmlReader.Element element) {
        int width = element.getIntAttribute("width", 0);
        int height = element.getIntAttribute("height", 0);
        int tileWidth = element.getParent().getIntAttribute("tilewidth", 0);
        int tileHeight = element.getParent().getIntAttribute("tileheight", 0);
        TiledMapTileLayer layer = new TiledMapTileLayer(width, height, tileWidth, tileHeight);

        loadBasicLayerInfo(layer, element);

        int[] ids = getTileIds(element, width, height);
        TiledMapTileSets tilesets = this.tilesets;
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                int id = ids[y * width + x];
                boolean flipHorizontally = ((id & FLAG_FLIP_HORIZONTALLY) != 0);
                boolean flipVertically = ((id & FLAG_FLIP_VERTICALLY) != 0);
                boolean flipDiagonally = ((id & FLAG_FLIP_DIAGONALLY) != 0);

                TiledMapTile tile = tilesets.getTile(id & ~MASK_CLEAR);
                if (tile != null) {
                    TiledMapTileLayer.Cell cell =
                            createTileLayerCell(flipHorizontally, flipVertically,
                                    flipDiagonally);
                    cell.setTile(tile);
                    layer.setCell(x, flipY ? height - 1 - y : y, cell);
                }
            }
        }

        XmlReader.Element properties = element.getChildByName("properties");
        if (properties != null) {
            loadProperties(layer.getProperties(), properties);
        }

        return layer;
    }

    private TiledMapTileLayer.Cell createTileLayerCell(boolean flipHorizontally,
                                                       boolean flipVertically,
                                                       boolean flipDiagonally) {
        TiledMapTileLayer.Cell cell = new TiledMapTileLayer.Cell();
        if (flipDiagonally) {
            if (flipHorizontally && flipVertically) {
                cell.setFlipHorizontally(true);
                cell.setRotation(TiledMapTileLayer.Cell.ROTATE_270);
            } else if (flipHorizontally) {
                cell.setRotation(TiledMapTileLayer.Cell.ROTATE_270);
            } else if (flipVertically) {
                cell.setRotation(TiledMapTileLayer.Cell.ROTATE_90);
            } else {
                cell.setFlipVertically(true);
                cell.setRotation(TiledMapTileLayer.Cell.ROTATE_270);
            }
        } else {
            cell.setFlipHorizontally(flipHorizontally);
            cell.setFlipVertically(flipVertically);
        }

        return cell;
    }

    private void loadBasicLayerInfo(MapLayer layer, XmlReader.Element element) {
        String name = element.getAttribute("name", null);
        float opacity = Float.parseFloat(element.getAttribute("opacity", "1.0"));
        boolean visible = element.getIntAttribute("visible", 1) == 1;

        layer.setName(name);
        layer.setOpacity(opacity);
        layer.setVisible(visible);
    }

    protected MapLayer loadObjectGroup(XmlReader.Element element) {
        String name = element.getAttribute("name", null);
        MapLayer layer = new MapLayer();
        layer.setName(name);
        XmlReader.Element properties = element.getChildByName("properties");
        if (properties != null) {
            loadProperties(layer.getProperties(), properties);
        }

        for (XmlReader.Element objectElement : element.getChildrenByName("object")) {
            MapObject object = loadObject(objectElement);
            layer.getObjects().add(object);
        }

        return layer;
    }

    private MapObject loadObject(XmlReader.Element element) {
        MapObject object = null;

        float scaleX = convertObjectToTileSpace ? 1.0f / mapTileWidth : 1.0f;
        float scaleY = convertObjectToTileSpace ? 1.0f / mapTileHeight : 1.0f;

        float x = element.getFloatAttribute("x", 0) * scaleX;
        float
                y =
                (flipY ?
                        (mapHeightInPixels - element.getFloatAttribute("y", 0)) :
                        element.getFloatAttribute("y", 0)) * scaleY;

        float width = element.getFloatAttribute("width", 0) * scaleX;
        float height = element.getFloatAttribute("height", 0) * scaleY;

        if (element.getChildCount() > 0) {
            XmlReader.Element child = null;
            if ((child = element.getChildByName("polygon")) != null) {
                String[] points = child.getAttribute("points").split(" ");
                float[] vertices = new float[points.length * 2];
                for (int i = 0; i < points.length; i++) {
                    String[] point = points[i].split(",");
                    vertices[i * 2] = Float.parseFloat(point[0]) * scaleX;
                    vertices[i * 2 + 1] =
                            Float.parseFloat(point[1]) * scaleY * (flipY ? -1 : 1);
                }
                Polygon polygon = new Polygon(vertices);
                polygon.setPosition(x, y);
                object = new PolygonMapObject(polygon);
            } else if ((child = element.getChildByName("polyline")) != null) {
                String[] points = child.getAttribute("points").split(" ");
                float[] vertices = new float[points.length * 2];
                for (int i = 0; i < points.length; i++) {
                    String[] point = points[i].split(",");
                    vertices[i * 2] = Float.parseFloat(point[0]) * scaleX;
                    vertices[i * 2 + 1] =
                            Float.parseFloat(point[1]) * scaleY * (flipY ? -1 : 1);
                }
                Polyline polyline = new Polyline(vertices);
                polyline.setPosition(x, y);
                object = new PolylineMapObject(polyline);
            } else if ((child = element.getChildByName("ellipse")) != null) {
                object = new EllipseMapObject(x, flipY ? y - height : y, width, height);
            }
        }
        if (object == null) {
            String gid = null;
            if ((gid = element.getAttribute("gid", null)) != null) {
                int id = (int) Long.parseLong(gid);
                boolean flipHorizontally = ((id & FLAG_FLIP_HORIZONTALLY) != 0);
                boolean flipVertically = ((id & FLAG_FLIP_VERTICALLY) != 0);

                TiledMapTile tile = tilesets.getTile(id & ~MASK_CLEAR);
                TiledMapTileMapObject tiledMapTileMapObject =
                        new TiledMapTileMapObject(tile, flipHorizontally, flipVertically);
                TextureRegion textureRegion = tiledMapTileMapObject.getTextureRegion();
                tiledMapTileMapObject.getProperties().put("gid", id);
                tiledMapTileMapObject.setX(x);
                tiledMapTileMapObject.setY(flipY ? y : y - height);
                float objectWidth =
                        element.getFloatAttribute("width", textureRegion.getRegionWidth());
                float objectHeight =
                        element.getFloatAttribute("height", textureRegion.getRegionHeight());
                tiledMapTileMapObject
                        .setScaleX(scaleX * (objectWidth / textureRegion.getRegionWidth()));
                tiledMapTileMapObject
                        .setScaleY(scaleY * (objectHeight / textureRegion.getRegionHeight()));
                tiledMapTileMapObject.setRotation(element.getFloatAttribute("rotation", 0));
                object = tiledMapTileMapObject;
            } else {
                object = new RectangleMapObject(x, flipY ? y - height : y, width, height);
            }
        }
        object.setName(element.getAttribute("name", null));
        String rotation = element.getAttribute("rotation", null);
        if (rotation != null) {
            object.getProperties().put("rotation", Float.parseFloat(rotation));
        }
        String type = element.getAttribute("type", null);
        if (type != null) {
            object.getProperties().put("type", type);
        }
        int id = element.getIntAttribute("id", 0);
        if (id != 0) {
            object.getProperties().put("id", id);
        }
        object.getProperties().put("x", x * scaleX);
        object.getProperties().put("y", (flipY ? y - height : y) * scaleY);
        object.getProperties().put("width", width);
        object.getProperties().put("height", height);
        object.setVisible(element.getIntAttribute("visible", 1) == 1);
        XmlReader.Element properties = element.getChildByName("properties");
        if (properties != null) {
            loadProperties(object.getProperties(), properties);
        }

        return object;
    }

    private void loadProperties(MapProperties properties, XmlReader.Element element) {
        if (element == null) {
            return;
        }
        if (element.getName().equals("properties")) {
            for (XmlReader.Element property : element.getChildrenByName("property")) {
                String name = property.getAttribute("name", null);
                String value = property.getAttribute("value", null);
                String type = property.getAttribute("type", null);
                if (value == null) {
                    value = property.getText();
                }
                Object castValue = castProperty(name, value, type);
                properties.put(name, castValue);
            }
        }
    }

    private Object castProperty(String name, String value, String type) {
        if (type == null) {
            return value;
        } else if (type.equals("int")) {
            return Integer.valueOf(value);
        } else if (type.equals("float")) {
            return Float.valueOf(value);
        } else if (type.equals("bool")) {
            return Boolean.valueOf(value);
        } else {
            throw new GdxRuntimeException(
                    "Wrong type given for property " + name + ", given : " + type
                            + ", supported : string, bool, int, float");
        }
    }
}