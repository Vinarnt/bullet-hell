package com.mygdx.game.timer;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.utils.Timer;
import com.gdx.extension.ScreenManager;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.mygdx.game.screen.ResultScreen;

public class LevelClearedTask extends Timer.Task {

    private ScreenManager screenManager;

    public LevelClearedTask(ScreenManager screenManager) {
        this.screenManager = screenManager;
    }

    @Override
    public void run() {
        Stage stage = screenManager.getStage();

        final VisLabel label = new VisLabel("Level Cleared", "title");
        final Container<VisLabel> container = new Container<VisLabel>(label);
        container.center();
        container.setFillParent(true);
        stage.addActor(container);

        label.addAction(
                Actions.sequence(
                        Actions.alpha(0f),
                        Actions.fadeIn(1f),
                        Actions.delay(2f),
                        Actions.fadeOut(1f),
                        Actions.run(
                                new Runnable() {
                                    @Override
                                    public void run() {
                                        screenManager.showScreen(ResultScreen.class);
                                    }
                                }
                        ),
                        Actions.removeActor()
                )
        );
    }
}