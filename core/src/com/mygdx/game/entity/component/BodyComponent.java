package com.mygdx.game.entity.component;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.utils.Pool;

public class BodyComponent implements Component, Pool.Poolable {

    private Body body;

    public BodyComponent() {

    }

    public Body getBody() {
        return body;
    }

    public BodyComponent setBody(Body body) {
        this.body = body;

        return this;
    }

    @Override
    public void reset() {
        body = null;
    }
}