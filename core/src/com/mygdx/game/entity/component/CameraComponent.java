package com.mygdx.game.entity.component;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.utils.Pool;

public class CameraComponent implements Component, Pool.Poolable {

    private OrthographicCamera camera;
    private float zoomFrom;
    private float zoomTo;
    private float zoom = 1f;

    public OrthographicCamera getCamera() {
        return camera;
    }

    public CameraComponent setCamera(OrthographicCamera camera) {
        this.camera = camera;

        return this;
    }

    public float getZoomFrom() {
        return zoomFrom;
    }

    public void setZoomFrom(float zoomFrom) {
        this.zoomFrom = zoomFrom;
    }

    public float getZoomTo() {
        return zoomTo;
    }

    public void setZoomTo(float zoomTo) {
        this.zoomTo = zoomTo;
    }

    public float getZoom() {
        return zoom;
    }

    public void setZoom(float zoom) {
        this.zoom = zoom;
    }

    @Override
    public void reset() {
        camera = null;
    }
}