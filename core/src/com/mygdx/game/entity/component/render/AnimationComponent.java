package com.mygdx.game.entity.component.render;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.utils.IntMap;
import com.badlogic.gdx.utils.Pool;

public class AnimationComponent implements Component, Pool.Poolable {

    private IntMap<Animation> animations = new IntMap<Animation>();

    public IntMap<Animation> getAnimations() {
        return animations;
    }

    public AnimationComponent addAnimation(int state, Animation animation) {
        animations.put(state, animation);

        return this;
    }

    @Override
    public void reset() {
        animations.clear();
    }
}