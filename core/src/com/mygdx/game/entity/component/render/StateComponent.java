package com.mygdx.game.entity.component.render;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.utils.Pool;

public class StateComponent implements Component, Pool.Poolable {

    private int state;
    private float time;

    public StateComponent() {

    }

    public float getTime() {
        return time;
    }

    public StateComponent addTime(float time) {
        this.time += time;

        return this;
    }

    public int getState() {
        return state;
    }

    public StateComponent setState(int state) {
        this.state = state;

        return this;
    }

    @Override
    public void reset() {
        state = 0;
        time = 0f;
    }
}