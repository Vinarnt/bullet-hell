package com.mygdx.game.entity.component.render;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Pool;

public class TextureComponent implements Component, Pool.Poolable {

    private TextureRegion texture;
    private float halfTextureWidth;
    private float halfTextureHeight;

    public TextureRegion getTexture() {
        return texture;
    }

    public TextureComponent setTexture(TextureRegion texture) {
        this.texture = texture;
        halfTextureWidth = texture.getRegionWidth() * 0.5f;
        halfTextureHeight = texture.getRegionHeight() * 0.5f;

        return this;
    }

    public float getHalfTextureWidth() {
        return halfTextureWidth;
    }

    public float getHalfTextureHeight() {
        return halfTextureHeight;
    }

    @Override
    public void reset() {
        texture = null;
        halfTextureHeight = 0f;
        halfTextureHeight = 0f;
    }
}