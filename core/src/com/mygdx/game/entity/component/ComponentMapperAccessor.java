package com.mygdx.game.entity.component;

import com.badlogic.ashley.core.ComponentMapper;
import com.mygdx.game.entity.component.ai.BehaviorComponent;
import com.mygdx.game.entity.component.ai.PathfindingComponent;
import com.mygdx.game.entity.component.ai.SteeringComponent;
import com.mygdx.game.entity.component.flag.BulletComponent;
import com.mygdx.game.entity.component.render.AnimationComponent;
import com.mygdx.game.entity.component.render.StateComponent;
import com.mygdx.game.entity.component.render.TextureComponent;
import com.mygdx.game.entity.component.transform.SpeedComponent;
import com.mygdx.game.entity.component.transform.TransformComponent;

public class ComponentMapperAccessor {

    public static final ComponentMapper<TransformComponent> TRANSFORM =
            ComponentMapper.getFor(TransformComponent.class);
    public static final ComponentMapper<SpeedComponent> SPEED =
            ComponentMapper.getFor(SpeedComponent.class);
    public static final ComponentMapper<InputControllerComponent> INPUT_CONTROLLER =
            ComponentMapper.getFor(InputControllerComponent.class);
    public static final ComponentMapper<CameraComponent> CAMERA =
            ComponentMapper.getFor(CameraComponent.class);
    public static final ComponentMapper<TargetComponent> TARGET =
            ComponentMapper.getFor(TargetComponent.class);
    public static final ComponentMapper<TextureComponent> TEXTURE =
            ComponentMapper.getFor(TextureComponent.class);
    public static final ComponentMapper<StateComponent> STATE =
            ComponentMapper.getFor(StateComponent.class);
    public static final ComponentMapper<AnimationComponent> ANIMATION =
            ComponentMapper.getFor(AnimationComponent.class);
    public static final ComponentMapper<BodyComponent> BODY =
            ComponentMapper.getFor(BodyComponent.class);
    public static final ComponentMapper<DelayComponent> DELAY =
            ComponentMapper.getFor(DelayComponent.class);
    public static final ComponentMapper<BulletComponent> BULLET =
            ComponentMapper.getFor(BulletComponent.class);
    public static final ComponentMapper<WeaponComponent> WEAPON =
            ComponentMapper.getFor(WeaponComponent.class);
    public static final ComponentMapper<DamageComponent> DAMAGE =
            ComponentMapper.getFor(DamageComponent.class);
    public static final ComponentMapper<HealthComponent> HEALTH =
            ComponentMapper.getFor(HealthComponent.class);
    public static final ComponentMapper<BehaviorComponent> BEHAVIOR =
            ComponentMapper.getFor(BehaviorComponent.class);
    public static final ComponentMapper<SteeringComponent> STEERING =
            ComponentMapper.getFor(SteeringComponent.class);
    public static final ComponentMapper<PathfindingComponent> PATHFINDING =
            ComponentMapper.getFor(PathfindingComponent.class);
}