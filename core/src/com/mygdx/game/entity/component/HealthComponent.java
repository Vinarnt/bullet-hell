package com.mygdx.game.entity.component;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.utils.Pool;

public class HealthComponent implements Component, Pool.Poolable {

    private float maxHealth;
    private float currentHealth;

    public HealthComponent() {
    }

    public float getCurrentHealth() {
        return currentHealth;
    }

    public HealthComponent setCurrentHealth(float currentHealth) {
        this.currentHealth = currentHealth;

        return this;
    }

    public float getMaxHealth() {
        return maxHealth;
    }

    public HealthComponent setMaxHealth(float maxHealth) {
        this.maxHealth = maxHealth;

        return this;
    }

    @Override
    public void reset() {
        currentHealth = 0f;
        maxHealth = 0f;
    }
}