package com.mygdx.game.entity.component;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.utils.Pool;

public class WeaponComponent implements Component, Pool.Poolable {

    private boolean shooting;
    private float shootDelay;
    private float untilNextShot;

    public WeaponComponent() {
    }

    public boolean isShooting() {
        return shooting;
    }

    public WeaponComponent setShooting(boolean shooting) {
        this.shooting = shooting;

        return this;
    }

    public float getShootDelay() {
        return shootDelay;
    }

    public WeaponComponent setShootDelay(float shootDelay) {
        this.shootDelay = shootDelay;

        return this;
    }

    public float getUntilNextShot() {
        return untilNextShot;
    }

    public WeaponComponent setUntilNextShot(float untilNextShot) {
        this.untilNextShot = untilNextShot;

        return this;
    }

    @Override
    public void reset() {
        shooting = false;
        shootDelay = 0f;
        untilNextShot = 0f;
    }
}