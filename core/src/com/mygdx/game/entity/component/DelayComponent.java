package com.mygdx.game.entity.component;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.utils.Pool;

public class DelayComponent implements Component, Pool.Poolable {

    private float delay;
    private float accumulator;

    public DelayComponent() {

    }

    public float getDelay() {
        return delay;
    }

    public DelayComponent setDelay(float delay) {
        this.delay = delay;

        return this;
    }

    public float getAccumulator() {
        return accumulator;
    }

    public void addAccumulator(float add) {
        accumulator += add;
    }

    @Override
    public void reset() {
        delay = 0f;
        accumulator = 0f;
    }
}