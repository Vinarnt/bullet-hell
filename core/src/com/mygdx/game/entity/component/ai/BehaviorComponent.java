package com.mygdx.game.entity.component.ai;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.ai.btree.BehaviorTree;
import com.badlogic.gdx.utils.Pool;
import com.mygdx.game.entity.system.ai.behavior.blackboard.Blackboard;

public class BehaviorComponent implements Component, Pool.Poolable {

    private BehaviorTree<? extends Blackboard> behaviorTree;

    public BehaviorComponent() {

    }

    public BehaviorTree<? extends Blackboard> getBehaviorTree() {
        return behaviorTree;
    }

    public <T extends Blackboard> BehaviorComponent setBehaviorTree(BehaviorTree<T> behaviorTree) {
        this.behaviorTree = behaviorTree;

        return this;
    }

    @Override
    public void reset() {
        this.behaviorTree = null;
    }
}