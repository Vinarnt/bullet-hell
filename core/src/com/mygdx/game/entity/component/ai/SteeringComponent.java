package com.mygdx.game.entity.component.ai;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.utils.Pool;
import com.mygdx.game.entity.system.ai.steering.SteeringBody;

public class SteeringComponent implements Component, Pool.Poolable {

    private SteeringBody steeringBody;

    public SteeringComponent() {

    }

    public SteeringBody getSteeringBody() {
        return steeringBody;
    }

    public SteeringComponent setSteeringBody(SteeringBody steeringBody) {
        this.steeringBody = steeringBody;

        return this;
    }

    @Override
    public void reset() {
        steeringBody = null;
    }
}