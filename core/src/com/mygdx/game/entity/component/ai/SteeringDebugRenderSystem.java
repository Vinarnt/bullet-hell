package com.mygdx.game.entity.component.ai;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.ai.steer.SteeringBehavior;
import com.badlogic.gdx.ai.steer.behaviors.CollisionAvoidance;
import com.badlogic.gdx.ai.steer.behaviors.PrioritySteering;
import com.badlogic.gdx.ai.steer.behaviors.Wander;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Disposable;
import com.mygdx.game.Constants;
import com.mygdx.game.entity.component.ComponentMapperAccessor;
import com.mygdx.game.entity.system.CameraSystem;
import com.mygdx.game.entity.system.ai.steering.BlendedSteering;
import com.mygdx.game.entity.system.ai.steering.SteeringBody;

public class SteeringDebugRenderSystem extends IteratingSystem implements Disposable {

    private static final ComponentMapper<SteeringComponent> steeringMapper =
            ComponentMapperAccessor.STEERING;

    private final ShapeRenderer shapeRenderer;
    private Camera camera;

    public SteeringDebugRenderSystem() {
        super(Family.all(SteeringComponent.class).get(), Constants.Systems.STEERING_DEBUG_RENDER);

        shapeRenderer = new ShapeRenderer();
        shapeRenderer.setAutoShapeType(true);
    }

    @Override
    public void addedToEngine(Engine engine) {
        super.addedToEngine(engine);

        camera = engine.getSystem(CameraSystem.class).getCamera();
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        SteeringBody steeringBody = steeringMapper.get(entity).getSteeringBody();
        SteeringBehavior<Vector2> steering = steeringBody.getSteeringBehavior();

        shapeRenderer.setProjectionMatrix(camera.combined);
        shapeRenderer.setColor(Color.RED);
        shapeRenderer.begin();
        drawBehavior(steering);
        shapeRenderer.end();
    }

    private void drawBehavior(SteeringBehavior<Vector2> steering) {
        if (!steering.isEnabled()) {
            return;
        }

        if (steering instanceof BlendedSteering) {
            BlendedSteering blendedSteering = (BlendedSteering) steering;
            for (int i = 0; i < blendedSteering.getSize(); i++) {
                drawBehavior(blendedSteering.get(i).getBehavior());
            }
        } else if (steering instanceof PrioritySteering) {

        } else if (steering instanceof Wander) {
            Wander<Vector2> wanderSteering = (Wander<Vector2>) steering;
            Vector2 wanderCenter = wanderSteering.getWanderCenter();
            shapeRenderer
                    .circle(wanderCenter.x, wanderCenter.y, wanderSteering.getWanderRadius(), 10);

            Vector2 targetPos = wanderSteering.getInternalTargetPosition();
            shapeRenderer.set(ShapeRenderer.ShapeType.Filled);
            shapeRenderer.circle(targetPos.x, targetPos.y, 0.5f, 10);
        } else if (steering instanceof CollisionAvoidance) {
            CollisionAvoidance<Vector2> collisionAvoidanceSteering =
                    (CollisionAvoidance<Vector2>) steering;
        }
    }

    @Override
    public void dispose() {
        shapeRenderer.dispose();
    }
}