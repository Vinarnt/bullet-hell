package com.mygdx.game.entity.component.ai;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Pool.Poolable;
import com.mygdx.game.entity.system.ai.pathfinding.TiledGraphPath;

public class PathfindingComponent implements Component, Poolable {

    private final Vector2 destination = new Vector2();
    private TiledGraphPath path;

    public PathfindingComponent() {
    }

    public Vector2 getDestination() {
        return destination;
    }

    public PathfindingComponent setDestination(Vector2 destination) {
        this.destination.set(destination);

        return this;
    }

    public TiledGraphPath getPath() {
        return path;
    }

    public void setPath(TiledGraphPath path) {
        this.path = path;
    }

    @Override
    public void reset() {
        destination.setZero();
        path.clear();
    }
}