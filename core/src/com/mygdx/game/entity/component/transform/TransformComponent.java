package com.mygdx.game.entity.component.transform;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Pool;

public class TransformComponent implements Component, Pool.Poolable {

    private final Vector3 position;
    private final Vector2 origin;
    private final Vector2 scale;
    private float rotation;

    public TransformComponent() {
        position = new Vector3();
        origin = new Vector2();
        scale = new Vector2(1f, 1f);
    }

    public Vector3 getPosition() {
        return position;
    }

    public TransformComponent setPosition(Vector3 position) {
        this.position.set(position);

        return this;
    }

    public TransformComponent setPosition(float x, float y) {
        this.position.x = x;
        this.position.y = y;

        return this;
    }

    public Vector2 getOrigin() {
        return origin;
    }

    public TransformComponent setOrigin(float originX, float originY) {
        origin.x = originX;
        origin.y = originY;

        return this;
    }

    public float getZIndex() {
        return position.z;
    }

    public TransformComponent setZIndex(float z) {
        this.position.z = z;

        return this;
    }

    public Vector2 getScale() {
        return scale;
    }

    public TransformComponent setScale(Vector2 scale) {
        this.scale.set(scale);

        return this;
    }

    public TransformComponent setScale(float x, float y) {
        this.scale.set(x, y);

        return this;
    }

    public float getRotation() {
        return rotation;
    }

    public TransformComponent setRotation(float rotation) {
        this.rotation = rotation % 360;

        return this;
    }

    @Override
    public void reset() {
        position.setZero();
        origin.setZero();
        scale.set(1f, 1f);
        rotation = 0f;
    }
}
