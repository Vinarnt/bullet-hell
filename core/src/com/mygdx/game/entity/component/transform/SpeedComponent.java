package com.mygdx.game.entity.component.transform;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.utils.Pool;

public class SpeedComponent implements Component, Pool.Poolable {

    private float speed = 1f;

    public SpeedComponent() {

    }

    public float getSpeed() {
        return speed;
    }

    public SpeedComponent setSpeed(float speed) {
        this.speed = speed;

        return this;
    }

    @Override
    public void reset() {
        this.speed = 1f;
    }
}