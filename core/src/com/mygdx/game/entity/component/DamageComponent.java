package com.mygdx.game.entity.component;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.utils.Pool;

public class DamageComponent implements Component, Pool.Poolable {

    private float damage;

    public DamageComponent() {

    }

    public float getDamage() {
        return damage;
    }

    public DamageComponent setDamage(float damage) {
        this.damage = damage;

        return this;
    }

    @Override
    public void reset() {
        damage = 0f;
    }
}