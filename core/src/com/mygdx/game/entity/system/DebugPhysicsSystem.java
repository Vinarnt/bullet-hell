package com.mygdx.game.entity.system;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Disposable;
import com.mygdx.game.Constants;
import com.mygdx.game.entity.component.CameraComponent;
import com.mygdx.game.entity.component.ComponentMapperAccessor;

public class DebugPhysicsSystem extends EntitySystem implements Disposable {

    private Box2DDebugRenderer debugRenderer;
    private World world;
    private OrthographicCamera camera;

    public DebugPhysicsSystem() {
        super(Constants.Systems.PHYSICS_DEBUG_RENDER);

        debugRenderer = new Box2DDebugRenderer();
    }

    @Override
    public void addedToEngine(Engine engine) {
        super.addedToEngine(engine);

        PhysicsSystem physSystem = engine.getSystem(PhysicsSystem.class);
        world = physSystem.getWorld();

        CameraSystem camSystem = engine.getSystem(CameraSystem.class);

        Entity cameraEntity = camSystem.getCameraEntity();
        CameraComponent camComp = ComponentMapperAccessor.CAMERA.get(cameraEntity);
        camera = camComp.getCamera();
    }

    @Override
    public void update(float deltaTime) {
        debugRenderer.render(world, camera.combined);
    }

    @Override
    public void dispose() {
        debugRenderer.dispose();
    }
}