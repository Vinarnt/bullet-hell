package com.mygdx.game.entity.system;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.mygdx.game.Constants;
import com.mygdx.game.entity.component.ComponentMapperAccessor;
import com.mygdx.game.entity.component.flag.BulletComponent;

public class BulletSystem extends IteratingSystem {

    private static final ComponentMapper<BulletComponent> bulletMapper =
            ComponentMapperAccessor.BULLET;

    public BulletSystem() {
        super(Family.all(BulletComponent.class).get(), Constants.Systems.BULLET);
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        if (!bulletMapper.get(entity).isAlive()) {
            getEngine().removeEntity(entity);
        }
    }
}
