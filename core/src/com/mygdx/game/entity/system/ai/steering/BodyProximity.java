package com.mygdx.game.entity.system.ai.steering;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.ai.steer.Proximity;
import com.badlogic.gdx.ai.steer.Steerable;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.QueryCallback;
import com.badlogic.gdx.physics.box2d.World;
import com.mygdx.game.entity.component.ComponentMapperAccessor;
import com.mygdx.game.entity.component.ai.SteeringComponent;

public class BodyProximity implements Proximity<Vector2>, QueryCallback {

    private static final ComponentMapper<SteeringComponent> steeringMapper =
            ComponentMapperAccessor.STEERING;

    private SteeringBody owner;
    private float detectionRadius;

    private ProximityCallback<Vector2> behaviorCallback;
    private int neighborCount;

    public BodyProximity(SteeringBody owner, float detectionRadius) {
        this.owner = owner;
        this.detectionRadius = detectionRadius;
    }

    @Override
    public Steerable<Vector2> getOwner() {
        return owner;
    }

    @Override
    public void setOwner(Steerable<Vector2> owner) {
        this.owner = (SteeringBody) owner;
    }

    public float getDetectionRadius() {
        return detectionRadius;
    }

    public void setDetectionRadius(float detectionRadius) {
        this.detectionRadius = detectionRadius;
    }

    @Override
    public int findNeighbors(ProximityCallback<Vector2> callback) {
        this.behaviorCallback = callback;
        neighborCount = 0;

        Vector2 position = owner.getPosition();
        float lowerX = position.x - detectionRadius;
        float lowerY = position.y - detectionRadius;
        float upperX = position.x + detectionRadius;
        float upperY = position.y + detectionRadius;

        World world = owner.getBody().getWorld();
        world.QueryAABB(this, lowerX, lowerY, upperX, upperY);
        this.behaviorCallback = null;

        return neighborCount;
    }

    @SuppressWarnings("unchecked")
    protected Steerable<Vector2> getSteerable(Fixture fixture) {
        Entity entity = (Entity) fixture.getBody().getUserData();
        SteeringComponent steeringComp = steeringMapper.get(entity);
        if (steeringComp != null) {
            if (owner.equals(steeringComp.getSteeringBody())) {
                return steeringComp.getSteeringBody();
            }
        }

        return null;
    }

    protected boolean accept(Steerable<Vector2> steerable) {
        return true;
    }

    @Override
    public boolean reportFixture(Fixture fixture) {
        Steerable<Vector2> steerable = getSteerable(fixture);
        if (steerable != null && steerable != owner && accept(steerable)) {
            if (behaviorCallback.reportNeighbor(steerable)) {
                neighborCount++;
            }
        }
        return true;
    }
}
