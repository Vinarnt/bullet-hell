package com.mygdx.game.entity.system.ai.steering;

import com.badlogic.gdx.ai.steer.Steerable;
import com.badlogic.gdx.math.Vector2;

public class BlendedSteering extends com.badlogic.gdx.ai.steer.behaviors.BlendedSteering<Vector2> {

    /**
     * Creates a {@code BlendedSteering} for the specified {@code owner}, {@code maxLinearAcceleration} and
     * {@code maxAngularAcceleration}.
     *
     * @param owner the owner of this behavior.
     */
    public BlendedSteering(Steerable<Vector2> owner) {
        super(owner);
    }

    public int getSize() {
        return list.size;
    }
}