package com.mygdx.game.entity.system.ai.behavior.blackboard;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.PooledEngine;

public class EnemyBlackboard extends Blackboard {

    private int step;

    public EnemyBlackboard(PooledEngine engine, Entity enemy) {
        super(engine, enemy);
    }

    public int getStep() {
        return step;
    }

    public void setStep(int step) {
        this.step = step;
    }
}