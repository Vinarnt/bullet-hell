package com.mygdx.game.entity.system.ai.task;

import com.badlogic.gdx.ai.btree.LeafTask;
import com.badlogic.gdx.ai.btree.Task;
import com.mygdx.game.entity.system.ai.behavior.blackboard.EnemyBlackboard;

public class ShootTask extends LeafTask<EnemyBlackboard> {

    @Override
    public Status execute() {
        EnemyBlackboard blackboard = getObject();
        return Status.SUCCEEDED;
    }

    @Override
    protected Task<EnemyBlackboard> copyTo(Task<EnemyBlackboard> task) {
        return task;
    }
}