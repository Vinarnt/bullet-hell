package com.mygdx.game.entity.system.ai.task;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.ai.btree.LeafTask;
import com.badlogic.gdx.ai.btree.Task;
import com.badlogic.gdx.ai.steer.behaviors.BlendedSteering;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.entity.component.ComponentMapperAccessor;
import com.mygdx.game.entity.component.ai.SteeringComponent;
import com.mygdx.game.entity.system.ai.behavior.blackboard.EnemyBlackboard;

public class PursueTask extends LeafTask<EnemyBlackboard> {

    private static final ComponentMapper<SteeringComponent> steeringMapper =
            ComponentMapperAccessor.STEERING;

    @Override
    public Status execute() {
        EnemyBlackboard blackboard = getObject();
        Entity enemy = blackboard.getEntity();
        SteeringComponent steeringComponent = steeringMapper.get(enemy);
        BlendedSteering<Vector2> steering =
                (BlendedSteering<Vector2>) steeringComponent.getSteeringBody()
                        .getSteeringBehavior();
        steering.get(0).getBehavior().setEnabled(true);

        return null;
    }

    @Override
    protected Task<EnemyBlackboard> copyTo(Task<EnemyBlackboard> task) {
        return task;
    }
}