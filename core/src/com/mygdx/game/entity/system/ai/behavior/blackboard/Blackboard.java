package com.mygdx.game.entity.system.ai.behavior.blackboard;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.PooledEngine;

public class Blackboard {

    private final PooledEngine engine;
    private final Entity entity;

    public Blackboard(PooledEngine engine, Entity entity) {
        this.engine = engine;
        this.entity = entity;
    }

    public PooledEngine getEngine() {
        return engine;
    }

    public Entity getEntity() {
        return entity;
    }
}