package com.mygdx.game.entity.system.ai.pathfinding;

import com.badlogic.gdx.utils.Array;

public class TiledNode {

    protected final int x;
    protected final int y;
    protected final boolean walkable;
    protected final Array<TiledConnection> connections;
    private final int index;

    public TiledNode(int x, int y, int graphHeight, boolean walkable) {
        this.x = x;
        this.y = y;
        this.index = x * graphHeight + y;
        this.walkable = walkable;

        this.connections = new Array<TiledConnection>((walkable) ? 8 : 0);
    }

    public int getIndex() {
        return index;
    }

    public void addConnection(TiledConnection connection) {
        connections.add(connection);
    }
}