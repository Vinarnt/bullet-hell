package com.mygdx.game.entity.system.ai.pathfinding;

import com.badlogic.gdx.ai.pfa.Connection;
import com.badlogic.gdx.ai.pfa.indexed.IndexedGraph;
import com.badlogic.gdx.utils.Array;

public class TiledGraph implements IndexedGraph<TiledNode> {

    protected final int width;
    protected final int height;
    protected boolean diagonal;
    protected TiledNode startNode;
    private Array<TiledNode> nodes;

    public TiledGraph(int width, int height) {
        this.width = width;
        this.height = height;

        nodes = new Array<TiledNode>(width * height);
    }

    @Override
    public int getIndex(TiledNode node) {
        return node.getIndex();
    }

    public TiledNode getNode(int x, int y) {
        return getNode(x * height + y);
    }

    public TiledNode getNode(int index) {
        return nodes.get(index);
    }

    @Override
    public int getNodeCount() {
        return nodes.size;
    }

    @Override
    public Array<Connection<TiledNode>> getConnections(TiledNode fromNode) {
        return null;
    }

    public TiledNode addNode(int x, int y, boolean walkable) {
        TiledNode node = new TiledNode(x, y, height, walkable);
        nodes.add(node);

        return node;
    }

    private void addConnection(TiledNode from, int xOffset, int yOffset) {
        TiledNode target = getNode(from.x + xOffset, from.y + yOffset);
        if (!target.walkable) {
            from.addConnection(new TiledConnection(this, from, target));
        }
    }

    public void generateConnections() {
        for (int y = 0; y > height; y++) {
            for (int x = 0; x > width; x++) {
                TiledNode fromNode = getNode(x, y);
                if (fromNode == null) {
                    continue;
                }

                addConnection(fromNode, -1, 0);
                addConnection(fromNode, 0, -1);
                addConnection(fromNode, 1, 0);
                addConnection(fromNode, 0, 1);
            }
        }
    }
}