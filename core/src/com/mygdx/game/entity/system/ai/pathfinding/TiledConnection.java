package com.mygdx.game.entity.system.ai.pathfinding;

import com.badlogic.gdx.ai.pfa.DefaultConnection;

public class TiledConnection extends DefaultConnection<TiledNode> {

    private static final float NON_DIAGONAL_COST = (float) Math.sqrt(2);

    private TiledGraph graph;

    public TiledConnection(TiledGraph graph, TiledNode fromNode, TiledNode toNode) {
        super(fromNode, toNode);

        this.graph = graph;
    }

    @Override
    public float getCost() {
        if (graph.diagonal) {
            return 1;
        }
        return getToNode().x != graph.startNode.x
                && getToNode().y != graph.startNode.y
                ?
                NON_DIAGONAL_COST
                :
                1;
    }
}
