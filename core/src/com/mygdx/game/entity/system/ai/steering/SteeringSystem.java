package com.mygdx.game.entity.system.ai.steering;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.mygdx.game.Constants;
import com.mygdx.game.entity.component.ComponentMapperAccessor;
import com.mygdx.game.entity.component.ai.SteeringComponent;

public class SteeringSystem extends IteratingSystem {

    private static final ComponentMapper<SteeringComponent> steeringMapper =
            ComponentMapperAccessor.STEERING;

    public SteeringSystem() {
        super(Family.all(SteeringComponent.class).get(), Constants.Systems.STEERING);
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        SteeringBody steeringBody = steeringMapper.get(entity).getSteeringBody();
        steeringBody.update(deltaTime);
    }
}