package com.mygdx.game.entity.system.ai.behavior;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.ai.GdxAI;
import com.mygdx.game.Constants;
import com.mygdx.game.entity.component.ComponentMapperAccessor;
import com.mygdx.game.entity.component.ai.BehaviorComponent;

public class BehaviorSystem extends IteratingSystem {

    private static final ComponentMapper<BehaviorComponent> behaviorMapper =
            ComponentMapperAccessor.BEHAVIOR;

    public BehaviorSystem() {
        super(Family.all(BehaviorComponent.class).get(), Constants.Systems.BEHAVIOR);
    }

    @Override
    public void update(float deltaTime) {
        GdxAI.getTimepiece().update(deltaTime);
        super.update(deltaTime);
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        behaviorMapper.get(entity).getBehaviorTree().step();
    }
}