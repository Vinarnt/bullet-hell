package com.mygdx.game.entity.system.ai.pathfinding;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.ai.pfa.Heuristic;
import com.badlogic.gdx.ai.pfa.indexed.IndexedAStarPathFinder;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.mygdx.game.Constants;
import com.mygdx.game.entity.component.ComponentMapperAccessor;
import com.mygdx.game.entity.component.ai.PathfindingComponent;
import com.mygdx.game.entity.component.transform.TransformComponent;

public class PathfindingSystem extends IteratingSystem {

    private static final ComponentMapper<TransformComponent> transformMapper =
            ComponentMapperAccessor.TRANSFORM;
    private static final ComponentMapper<PathfindingComponent> pathMapper =
            ComponentMapperAccessor.PATHFINDING;
    private final Heuristic<TiledNode> heuristic;
    private TiledGraph graph;
    private IndexedAStarPathFinder<TiledNode> pathfinder;

    public PathfindingSystem() {
        super(Family.all(PathfindingComponent.class).get(), Constants.Systems.PATHFINDING);

        heuristic = new ManathanDistance();
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        PathfindingComponent pathComp = pathMapper.get(entity);
        TransformComponent transformComp = transformMapper.get(entity);
        Vector3 startPoint = transformComp.getPosition();
        Vector2 endPoint = pathComp.getDestination();

        TiledNode startNode =
                graph.getNode(MathUtils.floor(startPoint.x), MathUtils.floor(startPoint.y));
        TiledNode endNode =
                graph.getNode(MathUtils.floor(endPoint.x), MathUtils.floor(endPoint.y));

        TiledGraphPath path = pathComp.getPath();
        pathfinder.searchNodePath(startNode, endNode, heuristic, path);
    }

    public void setGraph(TiledGraph graph) {
        this.graph = graph;
        graph.generateConnections();

        pathfinder = new IndexedAStarPathFinder<TiledNode>(graph);
    }
}