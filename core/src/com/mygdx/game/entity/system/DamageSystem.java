package com.mygdx.game.entity.system;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Timer;
import com.gdx.extension.ScreenManager;
import com.mygdx.game.Constants;
import com.mygdx.game.entity.component.ComponentMapperAccessor;
import com.mygdx.game.entity.component.DamageComponent;
import com.mygdx.game.entity.component.HealthComponent;
import com.mygdx.game.timer.LevelClearedTask;

public class DamageSystem extends IteratingSystem {

    private static final ComponentMapper<DamageComponent> damageMapper =
            ComponentMapperAccessor.DAMAGE;
    private static final ComponentMapper<HealthComponent> healthMapper =
            ComponentMapperAccessor.HEALTH;

    private final ScreenManager screenManager;

    public DamageSystem(ScreenManager screenManager) {
        super(Family.all().get(), Constants.Systems.DAMAGE);

        this.screenManager = screenManager;
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
    }

    public void inflictDamage(Entity damageFrom, final Entity target) {
        DamageComponent damageComp = damageMapper.get(damageFrom);
        HealthComponent healthComp = healthMapper.get(target);
        float health = healthComp.getCurrentHealth();
        health = MathUtils.clamp(health - damageComp.getDamage(), 0f, healthComp.getMaxHealth());
        healthComp.setCurrentHealth(health);

        if (health <= 0f) {
            getEngine().removeEntity(target);
            Timer.schedule(new LevelClearedTask(screenManager), 1f);
        }
    }
}