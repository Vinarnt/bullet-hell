package com.mygdx.game.entity.system;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.mygdx.game.Constants;
import com.mygdx.game.entity.component.ComponentMapperAccessor;
import com.mygdx.game.entity.component.render.AnimationComponent;
import com.mygdx.game.entity.component.render.StateComponent;
import com.mygdx.game.entity.component.render.TextureComponent;

public class AnimationSystem extends IteratingSystem {

    private static final ComponentMapper<TextureComponent> textureMapper =
            ComponentMapperAccessor.TEXTURE;
    private static final ComponentMapper<AnimationComponent> animationMapper =
            ComponentMapperAccessor.ANIMATION;
    private static final ComponentMapper<StateComponent> stateMapper =
            ComponentMapperAccessor.STATE;

    public AnimationSystem() {
        super(Family.all(TextureComponent.class,
                AnimationComponent.class,
                StateComponent.class).get(), Constants.Systems.ANIMATION);
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        TextureComponent texComp = textureMapper.get(entity);
        AnimationComponent animComp = animationMapper.get(entity);
        StateComponent stateComp = stateMapper.get(entity);

        Animation<TextureRegion> animation = animComp.getAnimations().get(stateComp.getState());
        if (animation != null) {
            TextureRegion texture = animation.getKeyFrame(stateComp.getTime());
            texComp.setTexture(texture);
        }

        stateComp.addTime(deltaTime);
    }
}