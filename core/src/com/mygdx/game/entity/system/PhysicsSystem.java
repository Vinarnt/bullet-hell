package com.mygdx.game.entity.system;

import com.badlogic.ashley.core.*;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.utils.ObjectMap;
import com.mygdx.game.Constants;
import com.mygdx.game.entity.component.BodyComponent;
import com.mygdx.game.entity.component.ComponentMapperAccessor;
import com.mygdx.game.entity.component.HealthComponent;
import com.mygdx.game.entity.component.transform.TransformComponent;
import com.mygdx.game.factory.PhysicsFactory;

public class PhysicsSystem extends IteratingSystem {

    private static final ComponentMapper<TransformComponent> transformMapper =
            ComponentMapperAccessor.TRANSFORM;
    private static final ComponentMapper<BodyComponent> bodyMapper =
            ComponentMapperAccessor.BODY;
    private static final ComponentMapper<HealthComponent> healthMapper =
            ComponentMapperAccessor.HEALTH;

    private static final float MAX_STEP_TIME = 1f / 60f;
    private final World world;
    private final ObjectMap<Entity, Body> bodies = new ObjectMap<Entity, Body>(64);
    private float accumulator;

    public PhysicsSystem() {
        super(Family.all(BodyComponent.class).get(), Constants.Systems.PHYSICS);

        world = new World(new Vector2(), true);
        world.setContactListener(new ContactListener() {
            @Override
            public void beginContact(Contact contact) {
                Fixture fixA = contact.getFixtureA();
                Fixture fixB = contact.getFixtureB();

                Entity entityA = (Entity) fixA.getBody().getUserData();
                Entity entityB = (Entity) fixB.getBody().getUserData();

                Entity bulletEntity = null;
                Entity targetEntity = null;
                if (ComponentMapperAccessor.BULLET.has(entityA)) {
                    bulletEntity = entityA;
                    targetEntity = entityB;
                } else if (ComponentMapperAccessor.BULLET.has(entityB)) {
                    bulletEntity = entityB;
                    targetEntity = entityA;
                }
                if (bulletEntity != null) {
                    ComponentMapperAccessor.BULLET.get(bulletEntity).kill();
                    if (healthMapper.has(targetEntity)) {
                        getEngine().getSystem(DamageSystem.class)
                                .inflictDamage(bulletEntity, targetEntity);
//                        contact.setEnabled(false);
                    }

                }
            }

            @Override
            public void endContact(Contact contact) {

            }

            @Override
            public void preSolve(Contact contact, Manifold oldManifold) {
            }

            @Override
            public void postSolve(Contact contact, ContactImpulse impulse) {
            }
        });
        PhysicsFactory.getInstance().setWorld(world);
    }

    @Override
    public void addedToEngine(Engine engine) {
        super.addedToEngine(engine);

        engine.addEntityListener(getFamily(), new EntityListener() {
            @Override
            public void entityAdded(Entity entity) {
                Body body = bodyMapper.get(entity).getBody();
                bodies.put(entity, body);
            }

            @Override
            public void entityRemoved(Entity entity) {
                Body body = bodies.remove(entity);
                world.destroyBody(body);
            }
        });
    }

    @Override
    public void update(float deltaTime) {
        float frameTime = Math.min(deltaTime, 0.25f);
        accumulator += frameTime;
        if (accumulator >= MAX_STEP_TIME) {
            world.step(MAX_STEP_TIME, 6, 2);
            accumulator -= MAX_STEP_TIME;
            super.update(deltaTime);
        }
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        TransformComponent transformComp = transformMapper.get(entity);
        BodyComponent bodyComp = bodyMapper.get(entity);

        Vector3 position = transformComp.getPosition();
        Body body = bodyComp.getBody();
        Vector2 bodyPosition = body.getPosition();
        position.x = bodyPosition.x;
        position.y = bodyPosition.y;
        transformComp.setRotation(body.getAngle() * MathUtils.radiansToDegrees);
    }

    public World getWorld() {
        return world;
    }
}