package com.mygdx.game.entity.system.input;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntityListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Body;
import com.mygdx.game.entity.component.BodyComponent;
import com.mygdx.game.entity.component.ComponentMapperAccessor;
import com.mygdx.game.entity.component.WeaponComponent;
import com.mygdx.game.entity.component.flag.PlayerComponent;
import com.mygdx.game.entity.component.transform.SpeedComponent;
import com.mygdx.game.entity.system.CameraSystem;

public class PlayerInputSystem extends InputSystem {

    private static final ComponentMapper<BodyComponent> bodyMapper =
            ComponentMapperAccessor.BODY;
    private static final ComponentMapper<SpeedComponent> speedMapper =
            ComponentMapperAccessor.SPEED;
    private static final ComponentMapper<WeaponComponent> weaponMapper =
            ComponentMapperAccessor.WEAPON;

    private static final Vector2 tmp = new Vector2();
    private static final Vector3 tmp2 = new Vector3();
    private static final float DASH_FORCE = 50000f;
    private final Vector2 force = new Vector2();
    private final Vector2 currentDashForce = new Vector2();
    private Entity player;
    private Camera camera;
    private boolean forward;
    private boolean backward;
    private boolean strafeLeft;
    private boolean strafeRight;

    public PlayerInputSystem() {
        super(PlayerComponent.class);

        inputs = new PlayerInputProcessor();
    }

    @Override
    public void addedToEngine(Engine engine) {
        super.addedToEngine(engine);

        engine.addEntityListener(getFamily(), new EntityListener() {
            @Override
            public void entityAdded(Entity entity) {
                player = entity;
            }

            @Override
            public void entityRemoved(Entity entity) {
                player = null;
            }
        });

        Entity cameraEntity = engine.getSystem(CameraSystem.class).getCameraEntity();
        camera = ComponentMapperAccessor.CAMERA.get(cameraEntity).getCamera();
    }

    @Override
    public void update(float deltaTime) {
        super.update(deltaTime);

        // Player orientation
        Body body = bodyMapper.get(player).getBody();
        Vector2 bodyPosition = body.getPosition();
        int mouseX = Gdx.input.getX();
        int mouseY = Gdx.input.getY();

        tmp2.setZero();
        tmp2.set(bodyPosition.x, bodyPosition.y, 0f);
        camera.project(tmp2);

        tmp.set(mouseX - tmp2.x, mouseY - tmp2.y);

        float angle = MathUtils.atan2(tmp.y, tmp.x);
        body.setTransform(body.getPosition(), -angle);

        // Movement
        force.setZero();
        final float speed = speedMapper.get(player).getSpeed();
        if (forward) {
            force.y += speed;
        }
        if (backward) {
            force.y -= speed;
        }
        if (strafeLeft) {
            force.x -= speed;
        }
        if (strafeRight) {
            force.x += speed;
        }

        // Dash
        if (currentDashForce.len() > 0f) {
            force.add(currentDashForce);
            currentDashForce.setZero();
        }

        body.applyForceToCenter(force.scl(body.getMass() * 10), true);
    }

    class PlayerInputProcessor implements InputProcessor {

        protected PlayerInputProcessor() {

        }

        @Override
        public boolean keyDown(int keycode) {
            if (player == null) {
                return false;
            }

            Body body = bodyMapper.get(player).getBody();
            float speed = speedMapper.get(player).getSpeed();

            switch (keycode) {
                case Input.Keys.Z:
                    forward = true;
                    return true;
                case Input.Keys.S:
                    backward = true;
                    return true;
                case Input.Keys.Q:
                    strafeLeft = true;
                    return true;
                case Input.Keys.D:
                    strafeRight = true;
                    return true;
                case Input.Keys.SHIFT_LEFT:
                    if (strafeLeft) {
                        currentDashForce.x = -DASH_FORCE;
                    } else if (strafeRight) {
                        currentDashForce.x = DASH_FORCE;
                    }

                    if (forward) {
                        currentDashForce.y = DASH_FORCE;
                    } else if (backward) {
                        currentDashForce.y = -DASH_FORCE;
                    }

                    if (currentDashForce.isZero()) {
                        currentDashForce.y = DASH_FORCE;
                    }
                    return true;
                default:
                    return false;
            }
        }

        @Override
        public boolean keyUp(int keycode) {
            if (player == null) {
                return false;
            }

            Body body = bodyMapper.get(player).getBody();
            float speed = speedMapper.get(player).getSpeed();

            switch (keycode) {
                case Input.Keys.Z:
                    forward = false;

                    return true;
                case Input.Keys.S:
                    backward = false;

                    return true;
                case Input.Keys.Q:
                    strafeLeft = false;

                    return true;
                case Input.Keys.D:
                    strafeRight = false;

                    return true;
                default:
                    return false;
            }
        }

        @Override
        public boolean keyTyped(char character) {
            return false;
        }

        @Override
        public boolean touchDown(int screenX, int screenY, int pointer, int button) {
            if (player == null) {
                return false;
            }

            switch (button) {
                case Input.Buttons.LEFT:
                    weaponMapper.get(player).setShooting(true);
                    return true;
                default:
                    return false;
            }
        }

        @Override
        public boolean touchUp(int screenX, int screenY, int pointer, int button) {
            if (player == null) {
                return false;
            }

            switch (button) {
                case Input.Buttons.LEFT:
                    weaponMapper.get(player).setShooting(false);
                    return true;
                default:
                    return false;
            }
        }

        @Override
        public boolean touchDragged(int screenX, int screenY, int pointer) {
            return false;
        }

        @Override
        public boolean mouseMoved(int screenX, int screenY) {
            return false;
        }

        @Override
        public boolean scrolled(int amount) {
            return false;
        }
    }
}