package com.mygdx.game.entity.system.input;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.MathUtils;
import com.mygdx.game.entity.component.CameraComponent;
import com.mygdx.game.entity.component.ComponentMapperAccessor;

public class CameraInputSystem extends InputSystem {

    private static final ComponentMapper<CameraComponent> cameraMapper =
            ComponentMapperAccessor.CAMERA;

    private static final float minZoom = 0.5f;
    private static final float maxZoom = 6f;
    private static final float zoomTime = 0.5f;
    private float zoomAmount;

    public CameraInputSystem() {
        super(CameraComponent.class);

        inputs = new CameraInputProcessor();
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        CameraComponent camComp = cameraMapper.get(entity);
        float currentZoom = camComp.getZoom();
        camComp.setZoom(MathUtils.clamp(currentZoom + zoomAmount, minZoom, maxZoom));
        zoomAmount = 0f;
    }

    class CameraInputProcessor implements InputProcessor {

        @Override
        public boolean keyDown(int keycode) {
            return false;
        }

        @Override
        public boolean keyUp(int keycode) {
            return false;
        }

        @Override
        public boolean keyTyped(char character) {
            return false;
        }

        @Override
        public boolean touchDown(int screenX, int screenY, int pointer, int button) {
            return false;
        }

        @Override
        public boolean touchUp(int screenX, int screenY, int pointer, int button) {
            return false;
        }

        @Override
        public boolean touchDragged(int screenX, int screenY, int pointer) {
            return false;
        }

        @Override
        public boolean mouseMoved(int screenX, int screenY) {
            return false;
        }

        @Override
        public boolean scrolled(int amount) {
            zoomAmount += amount;

            return true;
        }
    }
}