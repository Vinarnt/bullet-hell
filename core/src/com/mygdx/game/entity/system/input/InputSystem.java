package com.mygdx.game.entity.system.input;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.mygdx.game.Constants;
import com.mygdx.game.entity.component.InputControllerComponent;

public class InputSystem extends IteratingSystem implements InputProcessor {

    protected InputProcessor inputs;

    public InputSystem(Class<? extends Component> listenTo) {
        super(Family.all(listenTo, InputControllerComponent.class).get(), Constants.Systems.INPUT);
    }

    @Override
    public void addedToEngine(Engine engine) {
        super.addedToEngine(engine);

        ((InputMultiplexer) Gdx.input.getInputProcessor()).addProcessor(this);
    }

    @Override
    public void removedFromEngine(Engine engine) {
        super.addedToEngine(engine);

        ((InputMultiplexer) Gdx.input.getInputProcessor()).removeProcessor(this);
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
    }

    @Override
    public boolean keyDown(int keycode) {
        if (!checkProcessing()) {
            return false;
        } else {
            return inputs.keyDown(keycode);
        }
    }

    @Override
    public boolean keyUp(int keycode) {
        if (!checkProcessing()) {
            return false;
        } else {
            return inputs.keyUp(keycode);
        }
    }

    @Override
    public boolean keyTyped(char character) {
        if (!checkProcessing()) {
            return false;
        } else {
            return inputs.keyTyped(character);
        }
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if (!checkProcessing()) {
            return false;
        } else {
            return inputs.touchDown(screenX, screenY, pointer, button);
        }
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if (!checkProcessing()) {
            return false;
        } else {
            return inputs.touchUp(screenX, screenY, pointer, button);
        }
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        if (!checkProcessing()) {
            return false;
        } else {
            return inputs.touchDragged(screenX, screenY, pointer);
        }
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        if (!checkProcessing()) {
            return false;
        } else {
            return inputs.mouseMoved(screenX, screenY);
        }
    }

    @Override
    public boolean scrolled(int amount) {
        if (!checkProcessing()) {
            return false;
        } else {
            return inputs.scrolled(amount);
        }
    }
}