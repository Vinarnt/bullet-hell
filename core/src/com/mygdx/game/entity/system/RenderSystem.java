package com.mygdx.game.entity.system;

import com.badlogic.ashley.core.*;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.mygdx.game.Constants;
import com.mygdx.game.entity.component.CameraComponent;
import com.mygdx.game.entity.component.ComponentMapperAccessor;
import com.mygdx.game.entity.component.render.TextureComponent;
import com.mygdx.game.entity.component.transform.TransformComponent;

import java.util.Comparator;

public class RenderSystem extends IteratingSystem {

    private static final ComponentMapper<CameraComponent> cameraMapper =
            ComponentMapperAccessor.CAMERA;
    private static final ComponentMapper<TransformComponent> transformMapper =
            ComponentMapperAccessor.TRANSFORM;
    private static final ComponentMapper<TextureComponent> textureMapper =
            ComponentMapperAccessor.TEXTURE;

    private final Batch batch;
    private Array<Entity> renderQueue;
    private Comparator<Entity> comparator;

    private Entity camera;

    private ShapeRenderer debugRenderer;

    public RenderSystem(Batch batch) {
        super(Family.all(TextureComponent.class, TransformComponent.class).get(),
                Constants.Systems.RENDER);

        this.batch = batch;
        renderQueue = new Array<Entity>();

        comparator = new Comparator<Entity>() {
            @Override
            public int compare(Entity entityA, Entity entityB) {
                return (int) Math.signum(transformMapper.get(entityA).getZIndex() -
                        transformMapper.get(entityB).getZIndex());
            }
        };

        debugRenderer = new ShapeRenderer();
    }

    @Override
    public void addedToEngine(Engine engine) {
        super.addedToEngine(engine);

        camera = engine.getSystem(CameraSystem.class).getCameraEntity();
        engine.addEntityListener(getFamily(), new EntityListener() {
            @Override
            public void entityAdded(Entity entity) {
                renderQueue.add(entity);
            }

            @Override
            public void entityRemoved(Entity entity) {
                renderQueue.removeValue(entity, true);
            }
        });
    }

    @Override
    public void update(float deltaTime) {
        renderQueue.sort(comparator);

        CameraComponent cameraComp = cameraMapper.get(camera);
        batch.setProjectionMatrix(cameraComp.getCamera().combined);
        batch.begin();
        for (Entity entity : renderQueue) {
            processEntity(entity, deltaTime);
        }
        batch.end();

        debugRenderer.setProjectionMatrix(cameraComp.getCamera().combined);
        debugRenderer.begin(ShapeRenderer.ShapeType.Line);
        debugRenderer.rect(0f, 0f, 1f, 1f);
        debugRenderer.end();
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        TextureComponent texComp = textureMapper.get(entity);
        TextureRegion texture = texComp.getTexture();
        TransformComponent transformComp = transformMapper.get(entity);

        Vector3 position = transformComp.getPosition();
        Vector2 origin = transformComp.getOrigin();
        float rotation = transformComp.getRotation();
        Vector2 scale = transformComp.getScale();

        batch.setColor(Color.WHITE);
        batch.draw(texture,
                position.x - origin.x,
                position.y - origin.y,
                origin.x, origin.y,
                texture.getRegionWidth(), texture.getRegionHeight(),
                scale.x, scale.y,
                rotation);
    }
}