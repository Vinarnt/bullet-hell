package com.mygdx.game.entity.system;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.mygdx.game.Constants;
import com.mygdx.game.Textures;
import com.mygdx.game.entity.component.ComponentMapperAccessor;
import com.mygdx.game.entity.component.WeaponComponent;
import com.mygdx.game.entity.component.transform.TransformComponent;
import com.mygdx.game.factory.EntityFactory;

public class WeaponSystem extends IteratingSystem {

    private static final ComponentMapper<TransformComponent> transformMapper =
            ComponentMapperAccessor.TRANSFORM;
    private static final ComponentMapper<WeaponComponent> weaponMapper =
            ComponentMapperAccessor.WEAPON;

    private static final Vector2 tmp = new Vector2();

    public WeaponSystem() {
        super(Family.all(WeaponComponent.class).get(), Constants.Systems.WEAPON);
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        WeaponComponent weaponComp = weaponMapper.get(entity);
        float untilNextShot = weaponComp.getUntilNextShot();
        float shootDelay = weaponComp.getShootDelay();

        untilNextShot -= deltaTime;
        if (untilNextShot <= 0f && weaponComp.isShooting()) {
            untilNextShot = shootDelay + deltaTime;

            TransformComponent transformComp = transformMapper.get(entity);
            Vector3 position = transformComp.getPosition();
            tmp.set(position.x, position.y);
            tmp.add(new Vector2(2.3f, -0.4f).rotate(transformComp.getRotation()));
            EntityFactory.getInstance()
                    .createBullet(Textures.Regions.RED_BULLET, tmp.x, tmp.y,
                            transformComp.getRotation() *
                                    MathUtils.degreesToRadians);
        }

        weaponComp.setUntilNextShot(untilNextShot);
    }
}