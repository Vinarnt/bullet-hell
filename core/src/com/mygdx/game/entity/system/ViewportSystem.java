package com.mygdx.game.entity.system;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.mygdx.game.entity.component.CameraComponent;
import com.mygdx.game.entity.component.ComponentMapperAccessor;

public class ViewportSystem extends EntitySystem {

    private static final ComponentMapper<CameraComponent> cameraMapper =
            ComponentMapperAccessor.CAMERA;

    private Viewport viewport;

    public ViewportSystem() {
    }

    @Override
    public void addedToEngine(Engine engine) {
        CameraSystem cameraSystem = engine.getSystem(CameraSystem.class);
        Entity camera = cameraSystem.getCameraEntity();
        CameraComponent cameraComp = cameraMapper.get(camera);
        viewport = new ExtendViewport(30f, 30f, cameraComp.getCamera());
    }

    public void resize(int width, int height) {
        viewport.update(width, height);
    }
}