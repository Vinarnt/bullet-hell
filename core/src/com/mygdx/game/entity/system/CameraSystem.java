package com.mygdx.game.entity.system;

import com.badlogic.ashley.core.*;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector3;
import com.mygdx.game.Constants;
import com.mygdx.game.entity.component.CameraComponent;
import com.mygdx.game.entity.component.ComponentMapperAccessor;
import com.mygdx.game.entity.component.InputControllerComponent;
import com.mygdx.game.entity.component.TargetComponent;
import com.mygdx.game.entity.component.transform.TransformComponent;

public class CameraSystem extends IteratingSystem {

    private static final ComponentMapper<TransformComponent> transformMapper =
            ComponentMapperAccessor.TRANSFORM;
    private static final ComponentMapper<CameraComponent> camMapper =
            ComponentMapperAccessor.CAMERA;
    private static final ComponentMapper<TargetComponent> targetMapper =
            ComponentMapperAccessor.TARGET;

    private static final Vector3 tmp = new Vector3();

    private Entity camera;

    public CameraSystem() {
        super(Family.all(CameraComponent.class).one(TransformComponent.class, TargetComponent.class)
                        .get(),
                Constants.Systems.CAMERA);
    }

    @Override
    public void addedToEngine(Engine engine) {
        super.addedToEngine(engine);

        OrthographicCamera camera = new OrthographicCamera();

        PooledEngine poolEngine = (PooledEngine) engine;
        this.camera = poolEngine.createEntity();
        this.camera
                .add(poolEngine.createComponent(TransformComponent.class))
                .add(poolEngine.createComponent(CameraComponent.class).setCamera(camera))
                .add(poolEngine.createComponent(InputControllerComponent.class));
        poolEngine.addEntity(this.camera);
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        TransformComponent transformComp = transformMapper.get(entity);
        TargetComponent targetComp = targetMapper.get(entity);
        CameraComponent camComp = camMapper.get(entity);

        OrthographicCamera cam = camComp.getCamera();

        if (targetComp != null) {
            TransformComponent targetTransformComp = transformMapper.get(targetComp.getTarget());
            tmp.set(targetTransformComp.getPosition()).z = 0f;
        } else {
            tmp.set(transformComp.getPosition()).z = 0f;
        }

        cam.position.set(tmp);
        cam.zoom = camComp.getZoom();
        cam.update();
    }

    public Entity getCameraEntity() {
        return camera;
    }

    public OrthographicCamera getCamera() {
        return camMapper.get(camera).getCamera();
    }
}