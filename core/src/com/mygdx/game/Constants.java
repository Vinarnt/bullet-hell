package com.mygdx.game;

public class Constants {

    public static class World {

        public static final int PPM = 16;
        public static final float PPM_FACTOR = 1f / PPM;
    }

    public static class Systems {

        public static final int TIMER = 5;
        public static final int INPUT = 10;
        public static final int CAMERA = 10;
        public static final int BEHAVIOR = 15;
        public static final int PATHFINDING = 16;
        public static final int STEERING = 17;
        public static final int PHYSICS = 210;
        public static final int WEAPON = 240;
        public static final int ANIMATION = 250;
        public static final int RENDER = 1000;
        public static final int PHYSICS_DEBUG_RENDER = 1001;
        public static final int STEERING_DEBUG_RENDER = 1001;
        public static final int BULLET = 10000;
        public static final int DAMAGE = 10001;
    }
}