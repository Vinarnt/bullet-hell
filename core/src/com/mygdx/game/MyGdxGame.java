package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.gdx.extension.ScreenManager;
import com.kotcrab.vis.ui.VisUI;
import com.mygdx.game.screen.GameScreen;

public class MyGdxGame extends ApplicationAdapter {

    private AssetManager assetManager;
    private Viewport viewport;
    private Stage stage;
    private Batch batch;
    private ScreenManager screenManager;
    private InputMultiplexer inputs;

    @Override
    public void create() {
        viewport = new ScreenViewport();
        batch = new SpriteBatch();
        stage = new Stage(viewport, batch);
        inputs = new InputMultiplexer();
        inputs.addProcessor(stage);

        assetManager = new AssetManager();
        assetManager.load("skin/dark/dark.json", Skin.class);
        assetManager.finishLoading();

        Skin skin = assetManager.get("skin/dark/dark.json", Skin.class);
        VisUI.load(skin);

        Gdx.input.setInputProcessor(inputs);
        Gdx.gl.glClearColor(0f, 0f, 0f, 1f);

        screenManager = new ScreenManager(stage, skin, inputs);
        screenManager.registerScreen(new GameScreen(screenManager));
    }

    @Override
    public void render() {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        screenManager.render(Gdx.graphics.getDeltaTime());
    }

    @Override
    public void resize(int width, int height) {
        screenManager.resize(width, height);
    }

    @Override
    public void resume() {
        screenManager.resume();
    }

    @Override
    public void pause() {
        screenManager.pause();
    }

    @Override
    public void dispose() {
        assetManager.dispose();
        screenManager.dispose();
    }
}
