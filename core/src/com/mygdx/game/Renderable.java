package com.mygdx.game;

import com.badlogic.gdx.graphics.g2d.Batch;

public interface Renderable<T> extends Comparable<T> {

    void render(Batch batch);
}