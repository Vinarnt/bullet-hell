package com.mygdx.game;

public class Textures {

    public static class Regions {

        public static final String YELLOW_BULLET = "textures/bullets/yellow-bullet.png";
        public static final String RED_BULLET = "textures/bullets/red-bullet.png";
    }
}