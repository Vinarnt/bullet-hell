package com.mygdx.game.screen.ui;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.kotcrab.vis.ui.widget.VisProgressBar;
import com.mygdx.game.entity.component.ComponentMapperAccessor;
import com.mygdx.game.entity.component.HealthComponent;

public class EntityHealthBar extends VisProgressBar {

    private static final ComponentMapper<HealthComponent> healthMapper =
            ComponentMapperAccessor.HEALTH;

    private Entity target;

    public EntityHealthBar() {
        super(0f, 1f, 1f, false);

        setStepSize(1f);
    }

    @Override
    public void act(float delta) {
        if (target != null) {
            HealthComponent healthComp = healthMapper.get(target);
            if (healthComp != null) {
                setValue(healthComp.getCurrentHealth());
            }
        }

        super.act(delta);
    }

    public void setTarget(Entity target) {
        this.target = target;
        HealthComponent healthComp = healthMapper.get(target);
        setRange(0f, healthComp.getMaxHealth());
    }
}