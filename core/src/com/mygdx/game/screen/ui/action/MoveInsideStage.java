package com.mygdx.game.screen.ui.action;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.RelativeTemporalAction;

public class MoveInsideStage extends RelativeTemporalAction {

    private final Vector2 targetCoord = new Vector2();
    private final Vector2 stageCoord = new Vector2();
    private Stage stage;
    private float initialDistanceX;

    @Override
    protected void updateRelative(float percentDelta) {
        stage.getRoot().localToStageCoordinates(stageCoord);
        target.localToStageCoordinates(targetCoord);
        float moveBy = (stageCoord.x - targetCoord.x) * percentDelta;
        Gdx.app.debug("Action", "Total to move " + moveBy);
        target.moveBy(moveBy, 0f);
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }
}