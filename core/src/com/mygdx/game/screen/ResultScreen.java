package com.mygdx.game.screen;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Value;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.gdx.extension.BaseScreen;
import com.gdx.extension.ScreenManager;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisScrollPane;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.VisTextButton;
import com.mygdx.game.entity.system.input.InputSystem;

public class ResultScreen extends BaseScreen {

    private final Engine engine;

    public ResultScreen(final ScreenManager screenManager, Engine engine) {
        super(screenManager, 10);

        this.engine = engine;

        layout.center();
        VisTable table = new VisTable();
        table.setBackground("bg1");

        VisLabel title = new VisLabel("Result", "title");
        title.setAlignment(Align.center);
        table.add(title).growX().row();

        VerticalGroup verticalGroup = new VerticalGroup();
        verticalGroup.fill();
        for (int i = 0; i < 100; i++) {
            verticalGroup.addActor(new VisLabel("Line " + i));
        }
        VisScrollPane scrollPane = new VisScrollPane(verticalGroup, "default");
        scrollPane.setFadeScrollBars(false);
        scrollPane.setOverscroll(false, false);
        scrollPane.setSmoothScrolling(false);
        table.add(scrollPane).grow().row();

        VisTextButton continueButton = new VisTextButton("Continue");
        continueButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                layout.addAction(
                        Actions.sequence(
                                Actions.fadeOut(1f),
                                Actions.run(new Runnable() {
                                    @Override
                                    public void run() {
                                        screenManager.hideScreen(ResultScreen.this);
                                    }
                                })
                        )
                );
            }
        });
        table.add(continueButton).padBottom(20f);

        layout.add(table).size(Value.percentWidth(0.5f, layout), Value.percentHeight(0.7f, layout));
    }

    @Override
    protected void show() {
        layout.addAction(
                Actions.sequence(
                        Actions.alpha(0f),
                        Actions.fadeIn(1f)
                )
        );
        for (EntitySystem system : engine.getSystems()) {
            if (system instanceof InputSystem) {
                system.setProcessing(false);
            }
        }
    }

    @Override
    protected void hide() {
        for (EntitySystem system : engine.getSystems()) {
            if (system instanceof InputSystem) {
                system.setProcessing(true);
            }
        }
    }
}