package com.mygdx.game.screen;

import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.utils.Disposable;
import com.gdx.extension.BaseScreen;
import com.gdx.extension.ScreenManager;
import com.mygdx.game.LevelLoader;
import com.mygdx.game.entity.component.ai.SteeringDebugRenderSystem;
import com.mygdx.game.entity.system.*;
import com.mygdx.game.entity.system.ai.behavior.BehaviorSystem;
import com.mygdx.game.entity.system.ai.pathfinding.PathfindingSystem;
import com.mygdx.game.entity.system.ai.steering.SteeringSystem;
import com.mygdx.game.entity.system.input.CameraInputSystem;
import com.mygdx.game.entity.system.input.PlayerInputSystem;
import com.mygdx.game.factory.BehaviorFactory;
import com.mygdx.game.factory.EntityFactory;
import com.mygdx.game.factory.PhysicsFactory;
import com.mygdx.game.screen.ui.EntityHealthBar;

import java.io.IOException;

public class GameScreen extends BaseScreen {

    private final AssetManager assetManager;
    private final PooledEngine engine;

    private EntityHealthBar enemyHealthBar;

    public GameScreen(final ScreenManager screenManager) {
        super(screenManager, 1);

        Gdx.app.setLogLevel(Application.LOG_DEBUG);

        assetManager = new AssetManager();
        assetManager.setLoader(TiledMap.class, new TmxMapLoader(new InternalFileHandleResolver()));

        engine = new PooledEngine();
        engine.addSystem(new AnimationSystem());
        engine.addSystem(new CameraSystem());
        engine.addSystem(new ViewportSystem());
        engine.addSystem(new PhysicsSystem());
        engine.addSystem(new DebugPhysicsSystem());
        engine.addSystem(new RenderSystem(getScreenManager().getStage().getBatch()));
        engine.addSystem(new CameraInputSystem());
        engine.addSystem(new PlayerInputSystem());
        engine.addSystem(new WeaponSystem());
        engine.addSystem(new BulletSystem());
        engine.addSystem(new DamageSystem(screenManager));
        engine.addSystem(new BehaviorSystem());
        engine.addSystem(new SteeringSystem());
        engine.addSystem(new SteeringDebugRenderSystem());
        engine.addSystem(new PathfindingSystem());

        EntityFactory.getInstance().setAssetManager(assetManager);
        EntityFactory.getInstance().setEngine(engine);
        BehaviorFactory.getInstance().setEngine(engine);

        assetManager.load("textures/bullets/red-bullet.png", Texture.class);
        assetManager.load("textures/characters/survivor-idle-riffle.atlas", TextureAtlas.class);
        assetManager.finishLoading();

        loadUI();

        loadLevel("levels/level_001.tmx");

        screenManager.registerScreen(new ResultScreen(screenManager, engine), false);
    }

    private void loadUI() {
        enemyHealthBar = new EntityHealthBar();
        enemyHealthBar.setName("enemyHealthBar");
        enemyHealthBar.setValue(1f);

        layout.top();
        layout.add(enemyHealthBar).width(Gdx.graphics.getWidth() - 100f).padTop(20f);
    }

    private void loadLevel(final String levelPath) {
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                try {
                    LevelLoader levelLoader =
                            new LevelLoader(Gdx.files.internal(levelPath),
                                    assetManager,
                                    engine, screenManager.getStage());
                    levelLoader.load();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void render(float delta) {
        engine.update(delta);
    }

    @Override
    public void resize(int width, int height) {
        engine.getSystem(ViewportSystem.class).resize(width, height);
    }

    @Override
    public void dispose() {
        for (EntitySystem system : engine.getSystems()) {
            if (system instanceof Disposable) {
                ((Disposable) system).dispose();
            }
        }

        assetManager.dispose();
        engine.removeAllEntities();
        engine.clearPools();

        PhysicsFactory.getInstance().dispose();
    }
}