package com.mygdx.game.factory;

import com.badlogic.gdx.physics.box2d.*;

public class PhysicsFactory {

    private final PolygonShape bulletShape;
    private final PolygonShape rectangleShape;
    private World world;
    private PhysicsFactory() {
        bulletShape = new PolygonShape();
        rectangleShape = new PolygonShape();
    }

    public static PhysicsFactory getInstance() {
        return SingletonHolder.INSTANCE;
    }

    public void setWorld(World world) {
        this.world = world;
    }

    private Body createCharacter(float x, float y, float radius, int categoryBits) {
        CircleShape shape = new CircleShape();
        shape.setRadius(radius);

        FixtureDef fixDef = new FixtureDef();
        fixDef.shape = shape;
        fixDef.filter.categoryBits = (short) categoryBits;
        fixDef.density = 200f;

        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.position.set(x, y);
        bodyDef.fixedRotation = true;

        Body body = world.createBody(bodyDef);
        body.setLinearDamping(10f);
        body.createFixture(fixDef);

        shape.dispose();

        return body;
    }

    public Body createPlayer(float x, float y) {
        return createCharacter(x, y, 1f, Filter.PLAYER);
    }

    public Body createEnemy(float x, float y, float radius) {
        return createCharacter(x, y, radius, Filter.ENEMY);
    }

    public Body createBullet(float x, float y, float width, float height, float orientationInRad) {
        bulletShape.setAsBox(width, height);

        FixtureDef fixDef = new FixtureDef();
        fixDef.shape = bulletShape;
        fixDef.filter.categoryBits = Filter.BULLET;
        fixDef.filter.maskBits = Filter.OBSTACLE | Filter.ENEMY;

        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.position.set(x, y);
        bodyDef.angle = orientationInRad;
        bodyDef.fixedRotation = true;
        bodyDef.bullet = true;

        Body body = world.createBody(bodyDef);
        body.createFixture(fixDef);

        return body;
    }

    public Body createRectangleCollision(float x, float y, float width, float height) {
        FixtureDef fixDef = new FixtureDef();
        fixDef.shape = rectangleShape;
        rectangleShape.setAsBox(width, height);
        fixDef.filter.categoryBits = Filter.OBSTACLE;

        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.StaticBody;
        bodyDef.position.set(x, y);

        Body body = world.createBody(bodyDef);
        body.createFixture(fixDef);

        return body;
    }

    public void dispose() {
        bulletShape.dispose();
        rectangleShape.dispose();
    }

    static class Filter {

        private static final int OBSTACLE = 0x0001;
        private static final int BULLET = 0x0002;
        private static final int PLAYER = 0x0004;
        private static final int ENEMY = 0x0008;
        private static final int FILTER5 = 0x0010;
        private static final int FILTER6 = 0x0020;
        private static final int FILTER7 = 0x0040;
        private static final int FILTER8 = 0x0080;
        private static final int FILTER9 = 0x0100;
        private static final int FILTER10 = 0x0200;
        private static final int FILTER11 = 0x0400;
        private static final int FILTER12 = 0x0800;
        private static final int FILTER13 = 0x1000;
        private static final int FILTER14 = 0x2000;
        private static final int FILTER15 = 0x4000;
        private static final int FILTER16 = 0x8000;
    }

    private static class SingletonHolder {

        private static final PhysicsFactory INSTANCE = new PhysicsFactory();
    }
}