package com.mygdx.game.factory;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.gdx.ai.btree.BehaviorTree;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.mygdx.game.Animations;
import com.mygdx.game.Constants;
import com.mygdx.game.entity.component.*;
import com.mygdx.game.entity.component.ai.BehaviorComponent;
import com.mygdx.game.entity.component.ai.SteeringComponent;
import com.mygdx.game.entity.component.flag.BulletComponent;
import com.mygdx.game.entity.component.flag.PlayerComponent;
import com.mygdx.game.entity.component.render.AnimationComponent;
import com.mygdx.game.entity.component.render.StateComponent;
import com.mygdx.game.entity.component.render.TextureComponent;
import com.mygdx.game.entity.component.transform.SpeedComponent;
import com.mygdx.game.entity.component.transform.TransformComponent;
import com.mygdx.game.entity.system.CameraSystem;
import com.mygdx.game.entity.system.ai.behavior.blackboard.EnemyBlackboard;
import com.mygdx.game.entity.system.ai.steering.SteeringBody;

public class EntityFactory {

    private PooledEngine engine;
    private AssetManager assetManager;

    private EntityFactory() {
    }

    public static EntityFactory getInstance() {
        return SingletonHolder.INSTANCE;
    }

    public void setEngine(PooledEngine engine) {
        this.engine = engine;
    }

    public void setAssetManager(AssetManager assetManager) {
        this.assetManager = assetManager;
    }

    public Entity createPlayer(float x, float y) {
        TextureAtlas atlas = assetManager
                .get("textures/characters/survivor-idle-riffle.atlas", TextureAtlas.class);
        Animation animation = new Animation(0.10f, atlas.getRegions(), Animation.PlayMode.LOOP);

        Body playerBody = PhysicsFactory.getInstance().createPlayer(x, y);

        Entity player = engine.createEntity();
        playerBody.setUserData(player);
        player.add(
                engine.createComponent(TransformComponent.class)
                        .setOrigin(95f, 87f)
                        .setZIndex(11f)
                        .setScale(0.010f, 0.010f)
        )
                .add(engine.createComponent(SpeedComponent.class).setSpeed(40f))
                .add(engine.createComponent(AnimationComponent.class)
                        .addAnimation(Animations.IDLE, animation))
                .add(engine.createComponent(StateComponent.class).setState(Animations.IDLE))
                .add(engine.createComponent(TextureComponent.class))
                .add(engine.createComponent(InputControllerComponent.class))
                .add(engine.createComponent(BodyComponent.class).setBody(playerBody))
                .add(engine.createComponent(PlayerComponent.class))
                .add(engine.createComponent(WeaponComponent.class).setShootDelay(0.1f))
                .add(engine.createComponent(HealthComponent.class)
                        .setMaxHealth(100f)
                        .setCurrentHealth(100f));
        engine.addEntity(player);

        Entity camera = engine.getSystem(CameraSystem.class).getCameraEntity();
        camera.add(engine.createComponent(TargetComponent.class).setTarget(player));

        return player;
    }

    public Entity createEnemy(float x, float y, float radius) {
        TextureAtlas atlas = assetManager
                .get("textures/characters/survivor-idle-riffle.atlas", TextureAtlas.class);
        Animation animation = new Animation(0.10f, atlas.getRegions(), Animation.PlayMode.LOOP);

        Body enemyBody = PhysicsFactory.getInstance().createEnemy(x, y, radius);

        Entity enemy = engine.createEntity();
        enemyBody.setUserData(enemy);

        SteeringBody
                steeringBody =
                SteeringFactory.getInstance().createBossOneSteering(enemyBody, radius);
        EnemyBlackboard blackboard = new EnemyBlackboard(engine, enemy);

        BehaviorTree<EnemyBlackboard> behaviorTree =
                BehaviorFactory.getInstance().createBehaviorBossLevelOne(blackboard);

        enemy.add(engine.createComponent(TransformComponent.class)
                .setOrigin(95f, 87f)
                .setZIndex(11f)
                .setScale(0.010f * radius, 0.010f * radius))
                .add(engine.createComponent(SpeedComponent.class).setSpeed(20f))
                .add(engine.createComponent(AnimationComponent.class)
                        .addAnimation(Animations.IDLE, animation))
                .add(engine.createComponent(StateComponent.class).setState(Animations.IDLE))
                .add(engine.createComponent(TextureComponent.class))
                .add(engine.createComponent(BodyComponent.class).setBody(enemyBody))
                .add(engine.createComponent(HealthComponent.class)
                        .setMaxHealth(100f)
                        .setCurrentHealth(100f))
                .add(engine.createComponent(BehaviorComponent.class).setBehaviorTree(behaviorTree))
                .add(engine.createComponent(SteeringComponent.class).setSteeringBody(steeringBody));
        engine.addEntity(enemy);

        return enemy;
    }

    public Entity createRectangleCollision(float x, float y, float width, float height) {
        Body collisionBody =
                PhysicsFactory.getInstance().createRectangleCollision(
                        (x + (width * 0.5f)) * 4f,
                        (y + (height * 0.5f)) * 4f,
                        (width * 0.5f)  * 4f,
                        (height * 0.5f) * 4f
                );

        Entity collisionEntity = engine.createEntity();
        collisionBody.setUserData(collisionEntity);
        collisionEntity.add(engine.createComponent(TransformComponent.class))
                .add(engine.createComponent(BodyComponent.class).setBody(collisionBody));
        engine.addEntity(collisionEntity);

        return collisionEntity;
    }

    public Entity createBullet(String textureName, float x, float y, float orientationInRad) {
        Texture bulletTexture = assetManager.get(textureName, Texture.class);
        TextureRegion region = new TextureRegion(bulletTexture);

        final Body bulletBody =
                PhysicsFactory.getInstance()
                        .createBullet(x,
                                y,
                                bulletTexture.getWidth() * Constants.World.PPM_FACTOR,
                                bulletTexture.getHeight() * Constants.World.PPM_FACTOR,
                                orientationInRad);
        Vector2 velocity = bulletBody.getLinearVelocity();
        velocity.set(100f, 0f).rotateRad(orientationInRad);
        bulletBody.setLinearVelocity(velocity);

        Entity bulletEntity = engine.createEntity();
        bulletBody.setUserData(bulletEntity);
        bulletEntity
                .add(engine.createComponent(TransformComponent.class)
                        .setPosition(x, y)
                        .setScale(0.1f, 0.1f)
                        .setOrigin(region.getRegionWidth() * 0.5f,
                                region.getRegionHeight() * 0.5f)
                        .setZIndex(15f))
                .add(engine.createComponent(BodyComponent.class).setBody(bulletBody))
                .add(engine.createComponent(TextureComponent.class).setTexture(region))
                .add(engine.createComponent(SpeedComponent.class).setSpeed(100f))
                .add(engine.createComponent(BulletComponent.class))
                .add(engine.createComponent(DamageComponent.class).setDamage(10f));
        engine.addEntity(bulletEntity);

        return bulletEntity;
    }

    public Entity createTile(TextureRegion texture, int x, int y, int zindex) {
        Entity tile = engine.createEntity();
        tile
                .add(engine.createComponent(TransformComponent.class)
                        .setZIndex(zindex)
                        .setScale(Constants.World.PPM_FACTOR, Constants.World.PPM_FACTOR)
                        .setPosition(x * 4f, y * 4f))
                .add(engine.createComponent(TextureComponent.class).setTexture(texture));
        engine.addEntity(tile);

        return tile;
    }

    private static class SingletonHolder {

        private static final EntityFactory INSTANCE = new EntityFactory();
    }
}