package com.mygdx.game.factory;

import com.badlogic.gdx.ai.steer.behaviors.Wander;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.mygdx.game.entity.system.ai.steering.BlendedSteering;
import com.mygdx.game.entity.system.ai.steering.SteeringBody;

public class SteeringFactory {

    private SteeringFactory() {

    }

    public static SteeringFactory getInstance() {
        return SingletonHolder.INSTANCE;
    }

    public SteeringBody createBossOneSteering(Body body, float boundingRadius) {
        SteeringBody steeringBody = new SteeringBody(body, true, boundingRadius);
        steeringBody.setMaxLinearAcceleration(100f);
        steeringBody.setMaxLinearSpeed(20f);
        steeringBody.setMaxAngularAcceleration(10f);
        steeringBody.setMaxAngularSpeed(10f);

        BlendedSteering rootBehavior = new BlendedSteering(steeringBody);
        Wander<Vector2> wander = new Wander<Vector2>(steeringBody);
        wander.setFaceEnabled(false)
                .setWanderRadius(10f)
                .setWanderOffset(0f)
                .setWanderRate(10f);
        rootBehavior.add(wander, 1f);

        steeringBody.setSteeringBehavior(rootBehavior);

        return steeringBody;
    }

    private static class SingletonHolder {

        private static final SteeringFactory INSTANCE = new SteeringFactory();
    }
}