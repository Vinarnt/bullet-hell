package com.mygdx.game.factory;

import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.gdx.ai.btree.BehaviorTree;
import com.badlogic.gdx.ai.btree.branch.Selector;
import com.mygdx.game.entity.system.ai.behavior.blackboard.EnemyBlackboard;
import com.mygdx.game.entity.system.ai.task.ShootTask;

public class BehaviorFactory {

    private PooledEngine engine;

    private BehaviorFactory() {

    }

    public static BehaviorFactory getInstance() {
        return SingletonHolder.INSTANCE;
    }

    public BehaviorTree<EnemyBlackboard> createBehaviorBossLevelOne(EnemyBlackboard blackboard) {
        Selector<EnemyBlackboard> rootTask = new Selector<EnemyBlackboard>();
        rootTask.addChild(new ShootTask());

        BehaviorTree<EnemyBlackboard> behavior =
                new BehaviorTree<EnemyBlackboard>(rootTask, blackboard);

        return behavior;
    }

    public void setEngine(PooledEngine engine) {
        this.engine = engine;
    }

    private static class SingletonHolder {

        private static final BehaviorFactory INSTANCE = new BehaviorFactory();
    }
}